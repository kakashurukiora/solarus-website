<div align="center" style="margin-bottom: 1em;">
  <img src="icon.svg" width="100px"/>
</div>

# Solarus Website

[![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://www.gnu.org/copyleft/gpl.html)
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
[![Pipeline Status](https://gitlab.com/solarus-games/solarus-website/badges/master/pipeline.svg)](https://gitlab.com/solarus-games/solarus-website/commits/master)

Source code for the [Solarus website](https://www.solarus-games.org), built with the [Hugo](https://gohugo.io/) framework.

<div align="center">
  <img src="screenshot.webp" width="540px"/>
</div>

## 📦 Setup

First, install `hugo`. Be sure to use the `extended` version that has SCSS support (e.g. on Ubuntu):

```bash
snap install hugo --channel=extended
```

To run the website locally, go to the website directory, then type:

```bash
hugo server
```

## 🛠️ Build

To build the website to the `/public` directory, ready to be deployed:

```bash
rm -rf ./public
hugo --minify
```

## 🏗️ Continous Integration

The CI is configured to automate 2 tasks, when a commit is pushed (see [`.gitlab-ci.yml`](.gitlab-ci.yml)):

1. On all branches, it will build the website to check if there is no error.
2. On the default branch, it will build and deploy the website on GitLab Pages.

## ✨ Features

- Custom theme named `solarus`.
- English and French translations.
- French pages default to English if not translated.
- Solarus version and download packages are retrived from the `solarus.json` file.
- Static JSON API (More details in [the docs](CONTRIBUTING.md#json-static-api)):
- RSS feed (More details in [the docs](CONTRIBUTING.md#xml-static-rss-feed))):
- Local search, using the JSON API.

## ⚖️ License

The source code is licensed under the terms of the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.html) (GPL v3).

The resources are licensed under [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0/) (CC BY-SA 4.0).
