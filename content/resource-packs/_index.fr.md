---
title: Paquets de ressources
excerpt: Consultez les paquets de ressource pour Solarus.
tags: [paquet, ressource, paquets, ressources, paquet de ressource, resource packs, sprite, tileset, script]
aliases:
  - /fr/development/resource-packs
  - /fr/dev/resource-packs
layout: redirect
redirectUrl: "{{ get-config-param resourcePacksURL }}"
---
