---
date: '2017-05-09'
excerpt: Le monde de Mercuri's Chest est vaste, on vous l'avait annoncé. Aujourd'hui, Newlink vous présente cette toute capture d'écran toute fraùche du...
tags:
- solarus
title: Le village enneigé
---

Le monde de Mercuri's Chest est vaste, on vous l'avait annoncé. Aujourd'hui, Newlink vous présente cette toute capture d'écran toute fraîche du village dans les montagnes !

![snow](snow-300x227.png)

Les maps des diverses régions avancent petit à petit. Le désert, les forêts, la ville, le parc et autres. On est ambitieux pour ce projet, on a plein d'idées et ça prend forme peu à peu !
