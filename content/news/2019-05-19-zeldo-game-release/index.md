---
date: '2019-05-19'
excerpt: 'A new game made with Solarus has been released: the Chapter 2 of Le Défi de Zeldo!'
tags:
  - games
thumbnail: cover.jpg
title: Chapter 2 release for Le Défi de Zeldo
---

A new game made with Solarus is available since 05/10/2019. It's the second chapter for _Le Défi de Zeldo_ (Zeldo's Challenge), named _La Tour des Souvenirs_ (The Tower of Memories), and developped by only one person: ZeldoRetro !

![Logo](game-logo.png)

It's a game with often a humorous tone, including multiple references and characters from different universes. The length of play is rather short for those who'll only linger over the main quest, but the interest lies also in the various side quests that the game offers, and will give the most completionistic players a hard time.

![Screenshot 1](screen-1.png)

The game follows on from the first chapter, that ZeldoRetro published in 2017. This time, Zeldo, main antagonist and character on the cover picture, hopes to get his revenge and invited Link to go to the Tower of Memories, where he waits resolutely for him.

![Screenshot 2](screen-2.png)

You can download the game on [its page in the Solarus quest library](/en/games/defi-de-zeldo-ch-2). Count from 4 to 6 hours to complete the game. Enjoy !

{{< game-thumbnail "defi-de-zeldo-ch-2" >}}
