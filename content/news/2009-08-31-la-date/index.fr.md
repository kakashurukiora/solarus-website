---
date: '2009-08-31'
excerpt: Bonjour à tous, J'espère que vous avez passé un bon été.
tags:
  - solarus
title: La date !
---

Bonjour à tous,
J'espère que vous avez passé un bon été. Du côté de [Zelda Solarus DX](http://www.zelda-solarus.com/jeu-zsdx) en tout cas, ça a bien avancé, et y compris pendant les vacances ^\_^. C'est pourquoi le moment est venu de fixer et d'annoncer la date de sortie de la démo !

**Vendredi 18 décembre 2009**

Bien que la démo pourrait sortir plus tôt, j'ai décidé qu'il était préférable de prendre notre temps pour bien la tester et la peaufiner :). Comme ça, vous aurez une démo stable et, j'espère, de grande qualité. Si tout va bien, cela devrait également nous laisser le temps de préparer plusieurs versions selon les systèmes d'exploitation et les langues.

Il est prévu des traductions en anglais, en allemand et en italien. Si vous êtes volontaire pour effectuer une traduction dans une langue supplémentaire, [contactez-moi](http://www.zelda-solarus.com/contact.php), avec deux conditions : parler couramment la langue concernée... et être passionné(e) par Zelda :). Et pour les systèmes d'exploitation, je prévois de sortir la démo sous Windows, Linux et Mac OS X. Le code source de la démo sera disponible, donc vos contributions pour porter le jeu sur des systèmes exotiques ou ajouter des fonctionnalités pourront être envisagées.

La démo va inclure le début du jeu, jusqu'à la fin du premier donjon. Au niveau de l'avancement, j'ai profité de cet été pour compléter beaucoup de choses au niveau des maps mais aussi du moteur, avec de nombreuses corrections de bugs et de petites améliorations. Comme vous le verrez, il y a un certain souci du détail en ce qui concerne les animations, les sons et le gameplay. La démo va profiter de plusieurs nouveautés graphiques grâce au travail de Newlink, et entre nous, malgré les [captures d'écran](http://www.zelda-solarus.com/jeu-zsdx-images) déjà publiées, le meilleur reste à venir :P

Voilà, maintenant il ne reste plus qu'à compter les jours et à attendre impatiemment les vacances de Noël :mrgreen:
