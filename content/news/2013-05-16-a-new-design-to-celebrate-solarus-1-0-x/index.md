---
date: '2013-05-16'
excerpt: As you may have seen, Solarus 1.0.1 has just been released. The project is becoming quite big, as it incorporates an engine, 2 games (3 games soon)...
tags:
  - solarus
title: A new design to celebrate Solarus 1.0.x
---

As you may have seen, Solarus 1.0.1 has just been released. The project is becoming quite big, as it incorporates an engine, 2 games (3 games soon) and a quest editor. With the aim of increasing the project's visibility, an official logo has been made. We hope it will remind you vintage 90's video games companies logos. You can freely use it (both png and pixel-art) for your project as it is under [Creative Commons license](http://creativecommons.org/licenses/by/3.0/ 'Creative Commons License 3.0'), and download the [full pack](http://www.solarus-games.org/wp-content/uploads/2013/05/Logo-Solarus-Engine.7z 'full pack') of logos in EPS and PNG formats on the [Solarus logo page](http://www.solarus-games.org/solarus/logo/ 'Logo').

![solarus-logo-black-on-transparent](solarus-logo-black-on-transparent1-300x90.png)

![solarus-logo-white-on-black](solarus-logo-white-on-black-300x90.png)

![Solarus Engine logo in pixel-art, for title screens](title_screen_solarus_engine.png)

Consequently, the design for this blog has also been remade to match the logo's style. It uses the Presswork Wordpress theme which is a great theme if you want to build up quite fast and easily a blog. The pages have also been remade with more illustrations to improve navigation and readability.

Changes:

- New Wordpress theme : Presswork
- Responsive design
- Colors and design match the new logo style
- Illustrations, logos and artworks
- Christopho's tweets (at the bottom of the page)
- Page to download Solarus logo

We hope you'll like it. And now, here is a small bonus :

![titre-solarus-anim2](titre-solarus-anim2.gif)
