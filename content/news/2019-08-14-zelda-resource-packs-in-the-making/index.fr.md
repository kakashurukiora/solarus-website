---
date: '2019-08-14'
excerpt: Fatigués d'utiliser les ressources d'A Link to the Past et de voir encore et toujours les mêmes tilesets et sprites ?
tags:
- resource packs
thumbnail: cover.jpg
title: De nouveaux packs de ressources Zelda sont en préparation.
---

Depuis les débuts de Solarus, le pack de ressources principal a toujours été celui issu d'*A Link To The Past*. Mais pourquoi donc ? C'est parce que Solarus a été créé pour le jeu *Mystery of Solarus*, qui était une suite à *ALTTP*. Logiquement, c'est actuellement le pack de ressources le plus fourni, grâce à Medoc et d'autres contributeurs. Vous avez surement remarqué que plusieurs jeux utilisent ce pack de ressources.

Cependant, *ALTTP* n'est pas le seul et unique *Zelda* 2D en vu de dessus, et quelques personnes ont commencé à travailler sur des packs de ressources pour les autres jeux 2D de la série. C'est une bouffée d'air frais pour ceux qui commençaient à être lassés du style d'*ALTTP*.

Soyez conscients que ces packs de ressources ne sont pas finis, et ne proposent pour le moment que quelques tilesets et sprites. Votre aide est la bienvenue pour aider à les compléter.

### Pack de ressources pour The Legend of Zelda (NES, 1986)

C'est l'œuvre de Maloney and CopperKatana. Vous pouvez suivre l'avancement sur [le forum](http://forum.solarus-games.org/index.php/topic,1365.0.html). Ils vont même plus loin en ajoutant des objets, sons et musiques issues des jeux Zelda plus récents ! Si vous êtes à l'aise en programmation, ils cherchent de l'aide pour scripter les ennemis et les objets.

![Zelda 1](screenshot-zelda-1.png)

### Pack de ressources pour Oracle of Ages/Seasons (GBC, 2001)

C'est l'œuvre de Katsu. Malheureusement, le projet a été abandoné et à la recherche de personne motivées pour en reprendre le flambeau. Vous pouvez avoir davantage d'informations sur [le forum](http://forum.solarus-games.org/index.php/topic,1292.msg8091.html) ou sur [la page du pack de ressources](/fr/development/resource-packs/the-legend-of-zelda-oracles).

![Oracles](screenshot-oracles.png)

### Pack de ressources pour The Minish Cap (GBA, 2004)

Le chef d'œuvre du pixel art (sérieusement, admirez ce petit bijou !) a finalement son pack de ressources ! C'est l'œuvre de GregoryMcGregerson, et il a bien avancé. Malheureusement, le projet est en pause. L'auteur a tout placé sur [Github](https://github.com/GregoryMcGregerson/minish-resource-pack), donc n'hésitez pas à forker et améliorer ce pack de ressources. Vous pouvez avoir davantage d'informations sur [le forum](http://forum.solarus-games.org/index.php/topic,1346.0.html) ou sur [la page du pack de ressources](/fr/development/resource-packs/the-legend-of-zelda-the-minish-cap).

![Minish Cap](screenshot-minish-cap.png)

Espérons qu'on verra de moins en moins de clones d'*ALTTP* grâce à ces nouveaux packs de ressources !
