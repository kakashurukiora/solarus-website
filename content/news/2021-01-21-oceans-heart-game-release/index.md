---
date: '2021-01-21'
excerpt: Max Mraz made the first Solarus game to be released on Steam!
tags:
  - games
thumbnail: cover.jpg
title: Ocean's Heart game release
---

Max Mraz, who made quite a buzz with his previous game [Yarntown](/en/games/yarntown), was also working for a long time on a much bigger project: **Ocean's Heart**. The game has finally been published, after years of development. Max, with some help from the Solarus community and his publisher [Nordcurrent](https://www.nordcurrent.com/), is the first author to publish a commercial Solarus game on Steam.

Ocean’s Heart is an epic top-down action RPG in which you explore a beautiful archipelago as a young woman named Tilia. Take on contracts to fight monsters, descend deep into ancient dungeons, defeat menacing foes, and unravel the mystery of Ocean’s Heart. All done in beautiful, bright pixel art.

- See the game on [its page in the Solarus Quest Library](/en/games/oceans-heart).
- Download the game on [Steam](https://store.steampowered.com/app/1393750/Oceans_Heart/)

{{< youtube "jELtHA_VBJ0" >}}

{{< game-thumbnail "oceans-heart" >}}
