---
date: '2016-10-05'
excerpt: https://github.com/solarus-games Previously, the Git repositories were owned by christopho, our team leader. Please note that now, they have been...
tags:
  - solarus
title: Git repositories ownership change
---

Previously, the Git repositories were owned by **christopho**, our team leader. Please note that now, they have been transfered to **solarus-games** official Github account. Update your links according to the new adresse if needed.

- <https://github.com/solarus-games>
