---
date: '2013-02-27'
excerpt: 'Voici quelques nouvelles sur le développement de la nouvelle version du site. Ça avance bien : merci à Binbin, Renkineko, Valoo et Morwenn qui...'
tags:
  - solarus
title: Modernisation et recrutement
---

Voici quelques nouvelles sur le développement de la nouvelle version du site. Ça avance bien : merci à Binbin, Renkineko, Valoo et Morwenn qui contribuent à la programmation des nouvelles fonctionnalités et au basculement du contenu du site actuel vers le nouveau.

Le site sera plus moderne, plus vivant avec plus d?illustrations, plus de liens entre les différentes rubriques et plus d?interactivité. Et surtout, pour l?équipe, un vrai système d?administration moderne permettant d?ajouter et de modifier facilement des articles, des images, des solutions, des jeux, etc. Car jusqu?à présent, c?était tout le contraire : toute modification de n?importe quelle page du site (sauf les nouvelles sur la page d?accueil) devait être faite à la main et en passant par moi. Et moi, c?est bien connu que ma principale activité, c?est de créer des jeux amateurs et pas de mettre à jour les pages du site. (Demandez à Couet le temps que je mettais à publier les mangas :P). Il était grand temps de passer à l?ère moderne !

Dans la nouvelle version du site en préparation, les rédacteurs peuvent créer et éditer tout le contenu eux-mêmes et sans avoir besoin de connaître des langages informatiques, ce qui est bien plus confortable et donc efficace. Reste donc à trouver des rédacteurs ;).

Je recherche donc du monde à la fois pour aider à remplir la nouvelle version du site avec tout le contenu du site actuel, et aussi pour le mettre à jour après la sortie, aussi bien pour parler de l?actu Zelda que pour ajouter du contenu comme des soluces. [Contactez-moi](http://www.zelda-solarus.com/contact.php) si vous voulez donner un coup de main. Comme je le dis plus haut, aucune connaissance n?est nécessaire :).

Avec la nouvelle version du site devraient arriver quelques nouveaux contenus, notamment la solution écrite de [Zelda Mystery of Solarus DX](http://www.zelda-solarus.com/jeu-zsdx) (pour l?instant, elle existe en vidéo seulement). Cette solution, rédigée par Renkineko, sera très détaillée et avec un bestiaire des ennemis et des belles cartes.

Bref, il est certain qu?avec le nouveau site, les mises à jour seront plus faciles. Si on trouve des rédacteurs, elles seront plus fréquentes, ce qui redynamisera Zelda Solarus et le forum avec. Bref, ça dépend de vous !
