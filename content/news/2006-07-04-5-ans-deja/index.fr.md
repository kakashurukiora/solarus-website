---
date: '2006-07-04'
excerpt: Nous sommes le 4 juillet 2006. Cela fait aujourd'hui 5 ans que le site existe ! :o Pour fêter ça, je vous ai préparé un méga dossier qui...
tags:
  - solarus
title: 5 ans déjà !
---

Nous sommes le 4 juillet 2006. Cela fait aujourd'hui 5 ans que le site existe ! :o

Pour fêter ça, je vous ai préparé un méga dossier qui retrace l'historique du site durant ces 5 années. Depuis l'annonce de Zelda Solarus, jusqu'à l'E3 2006, en passant par les piratages et les problèmes d'hébergement, revivez toutes les grandes étapes de la vie de notre site. Vous verrez que ce dernier a vécu des périodes mouvementées... Et si le site est toujours là aujourd'hui c'est essentiellement grâce à vous :)

Les images de tous les anciens designs du site sont dans ce dossier, vous pourrez les voir au fur et à mesure de leur apparition dans la chronologie de l'historique.

En bonus, nous vous proposons même les images des designs qui n'ont pas été achevés, des images que vous n'avez jamais vues ! :D

Un grand merci à Metallizer pour m'avoir fourni la plupart des images des designs qui sont dans ce dossier.

[![](http://www.zelda-solarus.com/images/dossiers/5ans_site/zs1_mini.jpg)](http://www.zelda-solarus.com/dossiers.php?dossier=5ans_site)

[Voir le dossier : **Historique du site : 5 ans déjà !**](http://www.zelda-solarus.com/dossiers.php?dossier=5ans_site)

Joyeux anniversaire ZS ! ^\_^
