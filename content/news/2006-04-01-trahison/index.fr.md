---
date: '2006-04-01'
excerpt: J'ai réussi à poster ce message en trafiquant dans la base de données. Geomaster a en effet supprimé mon accès au site et m'a banni au forum, de...
tags:
  - solarus
title: Trahison !
---

J'ai réussi à poster ce message en trafiquant dans la base de données. Geomaster a en effet supprimé mon accès au site et m'a banni au forum, de même que les modérateurs. Et moi qui lui faisais confiance ! Jamais je n'aurais pensé qu'il me trahissait depuis toutes ses années. Il est passé du côté obscur de la Triforce et dès lors, il a oeuvré pour obtenir ma confiance pour mieux me contrôler, dans le seul but d'éradiquer Nintendo.

Par ailleurs, sachez que Super Tomate vient de nous livrer des photos donnant la preuve formelle que c'est Geomaster/Metallizer qui a volé les plans de Zelda Mercuris' Chest.

![](http://www.zelda-solarus.com/images/uploads/preuve_vol_zmc_geo.png)

![](http://www.zelda-solarus.com/images/uploads/hackpargeo.jpg)
