---
date: '2017-04-01'
excerpt: ' The incredible just happened. Today we are able to offer you, and waaaaaay before the announced release date of 2023, the final version of our...'
tags:
- solarus
title: Zelda Mercuris Chess is out !
---

![](2x-300x249.png)

The incredible just happened.

Today we are able to offer you, and waaaaaay before the announced release date of 2023, the final version of our new Zelda game: Mercuris Chess !

This is not without some emotion and proudness that we give you the opportunity to download our creation from now.

![](zsxd2_box-300x214.jpg)

#### [Download Zelda Mercuris Chess (full game)](http://www.solarus-games.org/games/zelda-xd2-mercuris-chess/)

Caution:
French only, Windows only (for the moment)

This ultimate version was made with long development nights, intense mapping, lots of coffee and Schoko-bons. We hope that you will enjoy this new 2D adventure which will make you travel through marvelous lands, wander complex dungeons and perhaps evoke some memories.

![](artwork_link-300x258.png)

Some of you may have guessed that a release was close, since Christopho published regularly screenshots of the game on his Twitter account since the begining of the year. Tell us, if you were right !

![](artwork_grump-292x300.png)

Don't hesitate to give us your appreciation in the comments or on the forum about this new adventure that closes years and years of work and passion.

Last but not least, we have to pay hommage to the whole Solarus Team who worked relentlessly on this crazy project. We can be proud of what we accomplished, guys ! Thank you all for the Game Jam memories, and congratulations for finishing this project.

See you and have fun !
