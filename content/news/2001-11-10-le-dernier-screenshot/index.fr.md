---
date: '2001-11-10'
excerpt: Je viens de me rendre compte que deux des cinq screenshots du donjon 6 étaient les mêmes dans la news d'hier ! Voici le screenshot manquant...
tags:
- solarus
title: Le dernier screenshot
---

Je viens de me rendre compte que deux des cinq screenshots du donjon 6 étaient les mêmes dans la news d'hier ! Voici le screenshot manquant :
![](solarus-ecran48.png)
Voilà ! Vous pouvez retrouver toutes les images du jeu dans la [galerie](http://www.zelda-solarus.com/galerie.php3).
