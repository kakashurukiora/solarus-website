---
date: '2009-02-17'
excerpt: Voici un petit point sur le développement du projet. Depuis la dernière mise à jour, plusieurs éléments du moteur ont été...
tags:
  - solarus
title: 'Zelda Solarus DX : dernières nouvelles'
---

Voici un petit point sur le développement du projet. Depuis la dernière mise à jour, plusieurs éléments du moteur ont été développés.

- **La séquence de game over** : quand Link n'a plus de coeur, il s'effondre. S'il avait une fée dans un flacon, elle vient le sauver en lui redonnant 7 coeurs, sinon le menu de game over s'affiche. Une nouveauté : si le joueur continue la partie, il recommence à la dernière entrée de maison ou de donjon.

- **Le terrain** : les décors ont depuis peu une notion de "terrain", par exemple de l'herbe, de l'eau profonde, de l'eau peu profonde ou bien sûr de la terre normale. Par exemple, si vous êtes sur de l'eau peu profonde, un peu d'eau vient s'afficher sous les pieds de Link et on entend un effet sonore de flaque quand il marche (c'est exactement comme dans A Link to the Past). Et si vous allez dans l'eau profonde, hum... voyez le point suivant ^\_^.

- **L'eau profonde** : Link peut maintenant sauter dans l'eau... et se noyer car les palmes ne sont pas encore faites ;).

- **Les objets "interactifs"** : ce sont les objets avec lesquels le joueur peut interagir en appuyant sur la touche Action lorsqu'il se trouve face à eux. En clair, ce sont par exemple les personnages du jeu avec qui il peut parler, les pancartes et de manière générale tous les événements qui peuvent se déclencher lorsque vous appuyez sur la touche Action. Et à chaque fois, l'icône bleue indique ce que vous pouvez faire ("Voir", "Parler"...), exactement comme dans Ocarina of Time. Bref, maintenant que ceci est fait, le créateur d'une map peut désormais y placer des personnages qui vont dialoguer avec le héros. Ca commence donc à prendre forme :).

- **La caméra** : il s'agit de la possibilité de déplacer la caméra d'un point à un autre, au lieu de la laisser centrée sur le héros comme c'est le cas en temps normal. Le script d'une map peut ainsi utiliser cette fonctionnalité pour interrompre le jeu et démarrer une cinématique. Avec ceci et le point précédent, on a à peu près tout ce qu'il faut pour faire l'intro du jeu.

Et voici en cadeau deux nouvelles images pour illustrer ces nouveautés :P

![](http://www.zelda-solarus.com/images/zsdx/sign.png)

![](http://www.zelda-solarus.com/images/zsdx/splash.png)
