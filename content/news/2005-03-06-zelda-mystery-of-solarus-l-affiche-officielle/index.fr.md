---
date: '2005-03-06'
excerpt: 'Bruno, déjà auteur de plusieurs fan arts, a réalisé rien que pour vous une affiche de notre création Zelda : Mystery of Solarus (cliquez pour...'
tags:
- solarus
title: 'Zelda Mystery of Solarus : l''affiche officielle !'
---

Bruno, déjà auteur de plusieurs [fan arts](http://www.zelda-solarus.com/fans.php), a réalisé rien que pour vous une affiche de notre création Zelda : Mystery of Solarus (cliquez pour agrandir) :

![](affiche_mini.jpg)

On espère qu'elle vous plaît, quoi qu'il en soit laissez vos commentaires !

Bruno est graphiste et peut prendre des contrats pour réaliser vos projets. Si vous souhaitez le contacter, [cliquez ici](http://www.zelda-solarus.com/mailto:brunodeconinck@hotmail.com).
