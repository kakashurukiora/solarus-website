---
date: '2023-08-28'
excerpt: Un stage d'une semaine, animé par Christopho, a permis à des enfants d'apprendre à créer un jeu.
tags:
  - games
  - solarus labs
thumbnail: thumbnail.png
title: Solarus a été utilisé pour enseigner le développement de jeux à des enfants
---

Christopho, le créateur de Solarus, a été invité par **le Numhérilab**, l’espace numérique de la [médiathèque d’Héricourt](https://mediatheque-payshericourt.c3rb.org/index.php) (Haute-Saône), pour faire découvrir à un petit groupe d’enfants passionnés de jeux vidéo la façon dont on les crée. Ce stage a été financé dans le cadre du [Contrat Territoire Lecture](https://www.culture.gouv.fr/Thematiques/Livre-et-lecture/Les-bibliotheques-publiques/Developpement-de-la-lecture-publique/Les-contrats-territoire-lecture-CTL) (CTL), un programme public mis en place à destination des bibliothèques.

![Photo prise pendant le stage (1)](numherilab_1.jpg)

Le stage a duré une semaine, fin août 2023, et a permis à 5 enfants d’apprendre à créer un jeu vidéo avec Solarus. Les enfants ont adoré, et ont appris beaucoup plus vite que prévu ! À la fin du stage, ils ont présenté leur jeu à leurs parents, qui ont pu constater les progrès faits en seulement une semaine. Peut-être bien que des vocations sont nées, car certains enfants aimeraient travailler dans la création de jeux vidéo plus tard.

Christopho a également apprécié animer le stage, car il aime faire de la pédagogie. Vous l’aviez peut-être constaté avec sa série de tutoriels sur Solarus ! Il a été secondé par Lionel Martin, animateur numérique, pour mener à bien le stage.

![Photo prise pendant le stage (2)](numherilab_2.jpg)

Ce fut une expérience concluante et très enrichissante pour tout le monde, et nous espérons qu’il y en aura d’autres à l’avenir, à Héricourt ou ailleurs.
