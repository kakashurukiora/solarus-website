---
date: '2023-04-09'
excerpt: Again a new patch of the demo!
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: "Bugfix release 0.1.2 for The Legend of Zelda: Mercuris' Chest demo"
---

## We hear you!

Many of you have once again reported bugs in the _Mercuris' Chest_ demo and we thank you for that.

We were able to compile a new version of the demo, titled **0.1.2**, it improves the quest and fixes bugs such as the walls of the Crater Temple or being able to buy potions, just that!

Go to the game page to play the demo:

{{< game-thumbnail "the-legend-of-zelda-mercuris-chest" >}}

## Still only in French?

Yes, this version does not contain English on-screen texts yet. We will soon announce our plan to bring you an English version of the game in the future weeks.

## Reminder: How to play

1. Download [Solarus Launcher](/en/download/#gamer-package).
2. Download [the game](/en/games/the-legend-of-zelda-mercuris-chest/) (as a `.solarus` file).
3. Open the `.solarus` file with Solarus Launcher.

We are still looking forward for your reactions and thoughs on our [Solarus Discord server](https://discord.gg/TPWDr3HG).

## Keep up reporting us bugs

Go to the [Gitlab issues of the demo](https://gitlab.com/solarus-games/games/zelda-mercuris-chest-demo/-/issues) page (Gitlab account required) or directly on [Discord](https://discord.gg/TPWDr3HG).

As always, be specific enough to help us best to reproduce the bug. If you want to report on Discord, remember to put your screenshots or explanations in spoiler if they risk revealing things to other players.
