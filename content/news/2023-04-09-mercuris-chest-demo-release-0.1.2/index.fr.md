---
date: '2023-04-09'
excerpt: Nouveau patch qui corrige quelques bugs
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: "Version corrective 0.1.2 de la démo de The Legend of Zelda: Mercuris' Chest"
---

## Nous vous avons compris !

Vous avez encore une fois été nombreux et nombreuses à nous rapporter des bugs dans la démo de _Mercuris' Chest_ et nous vous en remercions.

Nous avons pu compiler une nouvelle version de la démo, intitulée **0.1.2**, elle améliore la quête et corrige des bugs tels que les murs du temple du Cratère ou le fait de pouvoir acheter des potions, rien que ça !

Rendez-vous sur la page du jeu pour jouer à la démo :

{{< game-thumbnail "the-legend-of-zelda-mercuris-chest" >}}

## Rappel : Comment jouer

1. Téléchargez [Solarus Launcher](/fr/download/#gamer-package).
2. Téléchargez [le jeu](/fr/games/the-legend-of-zelda-mercuris-chest/) (sous forme de fichier `.solarus`).
3. Ouvrez le fichier `.solarus` avec Solarus Launcher.

Nous sommes toujours heureux de voir vos réactions sur la communauté [Discord Solarus](https://discord.gg/TPWDr3HG).

## Pour continuer à nous rapporter des bugs

Accéder à la page [issues Gitlab de la démo](https://gitlab.com/solarus-games/games/zelda-mercuris-chest-demo/-/issues) (compte Gitlab obligatoire) ou directement sur [Discord](https://discord.gg/TPWDr3HG).

Comme toujours, soyez précis pour nous aider au mieux à reproduire le bug. Si vous souhaitez passer par Discord, pensez à mettre vos screenshots ou explications en spoiler si elle risquent de divulgâcher les autres joueurs.
