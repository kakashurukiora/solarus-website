---
date: '2021-04-05'
excerpt: Solarus a été utilisé par l'INSEE dans le cadre d'une formation au langage R.
tags:
  - games
thumbnail: thumbnail.jpg
title: Solarus utilisé par l'INSEE
---

## Les acteurs du projet

Solarus a été utilisé par l'[INSEE](https://www.insee.fr) dans le cadre d'un projet pédagogique visant à enseigner le [langage R](https://www.r-project.org) de façon ludique et originale.

L'**INSEE**, Institut national de la statistique et des études économiques, est l'organisme public qui a pour rôle de réaliser des études statistiques en France.

![Logo de l'INSEE](insee-logo.png)

Le **langage R**, quant à lui, est un langage de programmation informatique adapté au calcul statistique. La société [ThinkR](https://thinkr.fr), spécialisée dans la formation sur R en France, a d'ailleurs accompagné le projet.

![Logo du langage R](r-logo.png)

Une équipe composée de statisticiens, géomaticiens, économistes et data scientists passionés de pédagogie s'est donc formée : Arnaud Degorre, Jean-Luc Lipatz, Sylvie Didier-Perot, Maël Theulière, Jérôme Baret, Olivier Chantrel, Julien Nicolas, Laure Genebes, Pascal Eusébio, Sébastien Terra, Fabrice Danielou, Diane Beldame et Vincent Guyader.

## Le projet

Le projet se nomme **Funcamp R** et vous pouvez le découvrir en détails et le télécharger sur son [site officiel](https://docs.funcamp.sspcloud.fr). Il consiste en un _serious game_ réalisé avec Solarus, et un site Internet companion. Le jeu est une aventure **Zelda** dans laquelle les énigmes sont liées au langage R et les statistiques. Son code est open-source et disponible sur [Github](https://github.com/InseeFrLab/funcamp-r-icarius).

![Capture d'écran de l'écran-titre du jeu](funcampr-screenshot.png)

## La bande-annonce

{{< youtube "Quz2CfF0gmo" >}}
