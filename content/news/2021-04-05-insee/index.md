---
date: '2021-04-05'
excerpt: Solarus was used by the INSEE as part of R language training.
tags:
  - games
thumbnail: thumbnail.jpg
title: Solarus was used by the INSEE
---

## The actors of the project

Solarus has been used by the [INSEE](https://www.insee.fr/) as part of an educational project aimed at teaching the [R language](https://www.r-project.org) in a playful and original way.

The **INSEE**, National Institute of Statistics and Economic Studies, is the public body whose role is to carry out statistical studies in France.

![INSEE logo](insee-logo.png)

The **R language**, on the other hand, is a computer programming language suited for statistical calculations. The company [ThinkR](https://thinkr.fr), specialized in R training in France, has also supported the project.

![R language logo](r-logo.png)

A team of statisticians, geomaticians, economists and data scientists with a passion for education was therefore formed: Arnaud Degorre, Jean-Luc Lipatz, Sylvie Didier-Perot, Maël Theulière, Jérôme Baret, Olivier Chantrel, Julien Nicolas, Laure Genebes, Pascal Eusébio, Sébastien Terra, Fabrice Danielou, Diane Beldame and Vincent Guyader.

## The project

The project is called **Funcamp R** and you can discover it in detail and download it on its [official website](https://docs.funcamp.sspcloud.fr). It consists of a _serious game_ made with Solarus, and a companion website. The game is a **Zelda** adventure in which the puzzles are linked to the R language and statistics. Its code is open-source and available on [Github](https://github.com/InseeFrLab/funcamp-r-icarius).

![Game title screen screenshot](funcampr-screenshot.png)

## The teaser

{{< youtube "Quz2CfF0gmo" >}}
