---
date: '2010-03-30'
excerpt: 'We have just released a new trailer of Zelda: Mystery of Solarus DX. It contains various elements of the beginning of the game, including small...'
tags:
- solarus
title: 'Zelda: Mystery of Solarus DX new trailer '
---

We have just released a new trailer of [Zelda: Mystery of Solarus DX](http://www.solarus-engine.org/zelda-mystery-of-solarus-dx). It contains various elements of the beginning of the game, including small details (such as walking on stairs and tapping against a wall with the sword), the fight with the boss of the first dungeon, and a sequence of the second dungeon (which is not present in the demo version). The music of this trailer is an original creation of our team!

{{< youtube "HUMcFAdBJ_g" >}}

I hope you will enjoy it!
