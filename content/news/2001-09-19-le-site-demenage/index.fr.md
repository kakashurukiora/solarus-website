---
date: '2001-09-19'
excerpt: Vous le savez probablement déjà, Babeloueb a annoncé sa fermeture définitive pour la fin du mois ! Alors apprenant cette nouvelle je n'ai pas...
tags:
- solarus
title: Le site déménage !
---

Vous le savez probablement déjà, Babeloueb a annoncé sa fermeture définitive pour la fin du mois !

Alors apprenant cette nouvelle je n'ai pas attendu que le site devienne une erreur 404 pour déménager sur Free. Un grand merci à Netgamer de [Oxyd-Online](http://www.oxyd-online.fr.st) pour son petit coup de pouce ! Tout fonctionne normalement, mis à part quelques détails qui vont être très vite réglés (ce soir vers 19 h) : le téléchargement de la [démo](http://www.zelda-solarus.com/download.php3?name=demo), des [RTP](http://www.zelda-solarus.com/download.php3?name=RTP) et des [polices](http://www.zelda-solarus.com/download.php3?name=polices_icone) d'écran françaises à rétablir, les bannières de pub de Babeloueb à supprimer, et je crois que je n'ai rien oublié. Si vous rencontrez cependant le moindre bug, signalez-le moi sur le [forum](http://www.zelda-solarus.com/forum.php3) ou par e-mail !

D'autre part, je souhaite bon courage aux 2000 sites qui sont comme moi obligés de déménager. Je tiens aussi malgré tout à remercier Babeloueb pour leur hébergement de qualité.
