---
date: '2002-06-20'
excerpt: En vue d'une nouvelle version du jeu, comportant le moins possible de bugs, nous avons besoin de votre aide. Venez tous sur le forum pour nous dire...
tags:
- solarus
title: 'Zelda Solarus : donnez-nous les bugs !'
---

En vue d'une nouvelle version du jeu, comportant le moins possible de bugs, nous avons besoin de votre aide. Venez tous sur le [forum](http://www.retrogame.com/~zsforums/viewtopic.php?t=224) pour nous dire tous les bugs que vous avez trouvés ou que vous trouverez dans Zelda Solarus. Plus vous serez nombreux à répondre, et meilleure sera la qualité de la version corrigée. Nous vous remercions d'avance !
