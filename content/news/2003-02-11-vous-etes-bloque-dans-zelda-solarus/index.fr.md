---
date: '2003-02-11'
excerpt: 'Depuis que nous avons supprimé la soluce du site, on reçoit plein d''e-mails de personnes bloquées dans Zelda : Mystery of Solarus. C''était...'
tags:
- solarus
title: Vous êtes bloqué dans Zelda Solarus ?
---

Depuis que nous avons supprimé la soluce du site, on reçoit plein d'e-mails de personnes bloquées dans Zelda : Mystery of Solarus. C'était prévisible, mais comme j'ai eu pitié de vous, j'accepte finalement de vous aider.

Nous n'avons cependant pas remis en ligne la soluce, car nous ne voulons pas gâcher la durée de vie du jeu. A la place de la soluce, nous avons donc fait une page qui répertorie les endroits où vous êtes le plus fréquemment bloqués, d'après les e-mails que vous nous envoyez et les messages du forum.

Pour chacun de ces endroits, on vous donne une indication qui vous aidera un peu. Un petit coup de pouce en quelque sorte :-)

[Déblocage Zelda Solarus](http://www.zelda-solarus.net/jeux.php?jeu=zs&zone=deblocage)
