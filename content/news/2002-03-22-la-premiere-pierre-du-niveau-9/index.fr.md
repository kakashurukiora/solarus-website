---
date: '2002-03-22'
excerpt: Salut à tous !!! Après des semaines d'attente, le grand moment est enfin arrivé ! Netgamer a terminé la conception du neuvième et dernier...
tags:
- solarus
title: La première pierre du niveau 9 !
---

Salut à tous !!!

Après des semaines d'attente, le grand moment est enfin arrivé ! Netgamer a terminé la conception du neuvième et dernier donjon. Il m'a donc envoyé son travail, accompagné d'un guide très détaillé pour que je puisse programmer exactement ce qu'il veut. Nous avons passé plus de deux heures cet après-midi en conversation vocale pour mettre tout ça au point. La programmation va donc commencer dès ce soir, et j'espère la finir en moins de deux semaines. Ce qui nous laissera encore une semaine avant le 15 avril, date à laquelle la programmation de tout le jeu devrait être terminée. Ensuite il faudra mettre au point le programme d'installation, celui de lancement du jeu, la notice, et... je ne vous en dis pas plus !

Bref, nous pouvons maintenant vous annoncer que sauf incident la date du 26 avril sera respectée !
