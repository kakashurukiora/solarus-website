---
date: '2001-07-03'
excerpt: Ouverture du forum spécial Zelda Solarus ! Venez nombreux pour donner vos impressions ou vos suggestions sur le jeu ! Visiter&nbsp;le&nbsp;forum.
tags:
- solarus
title: Ouverture du forum
---

Ouverture du forum spécial Zelda Solarus ! Venez nombreux pour donner vos impressions ou vos suggestions sur le jeu !

[Visiter le forum](http://www.zelda-solarus.com/forum.php3).
