---
date: '2002-10-22'
excerpt: Salut à tous ! Après deux jours de panique, tout est rétabli (du moins provisoirement) ! DaN, un membre de l'équipe de développement de Zelda...
tags:
  - solarus
title: Nous revoilà !
---

Salut à tous !

Après deux jours de panique, tout est rétabli (du moins provisoirement) ! DaN, un membre de l'équipe de développement de Zelda Solarus 2, nous héberge temporairement, jusqu'à ce que nous trouvions une vraie solution. Nous ne le remercierons jamais assez... Tout était heureusement sauvegardé sur nos disques. La dernière sauvegarde de la base de données datait de vendredi dernier, c'est donc un énorme coup de chance car je le fais très rarement. Seule la dernière news (à propos de la newsletter) et les commentaires des news ont donc disparu.

L'adresse du site reste <http://www.zelda-solarus.fr.st> et rien d'autre ! L'url qui se cache derrière cette adresse est de toute façon temporaire. Quels que soient les changements d'hébergeurs, cette adresse a l'avantage de rester toujours valable.

Vous voudriez sans doute savoir pourquoi zeldasolarus.free.fr affichait "erreur 403" depuis hier matin. Ca tombe bien, nous venons tout juste de recevoir la réponse de Free. Je cite leur e-mail :

> Bonjour,
>
> Votre site à été bloqué pour l'une des raisons suivante :
>
> - Abus de ressource système :
>
> **Site générant trop de trafic**. Dégradant ainsi la qualité de nos services.

D'autres raisons sont citées ensuite, mais on sait que la vraie raison est la trop forte audience du site. Webmasters de sites hébergés par Free, prenez donc ceci comme un avertissement : évitez d'avoir trop de succès ! C'est interdit par votre hébergeur ! :D
