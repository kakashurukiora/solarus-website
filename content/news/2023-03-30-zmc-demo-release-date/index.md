---
date: '2023-03-30'
excerpt: Mercuris' Chest is getting a demo, as previously announced in 2002.
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: "Release date for the demo of The Legend of Zelda: Mercuris' Chest"
---

## An Announcement From 2002

Remember, [in 2002](/fr/news/2002-07-23-zelda-solarus-2), the team behind _Zelda: Mystery of Solarus_ loudly announced its next game: _Mercuris' Chest_.

A demo was even released for that occasion, made with [The Games Factory](https://www.clickteam.com/fr/the-games-factory-2) then [Multimedia Fusion](https://www.clickteam.com/multimedia-fusion-2). It was long before the creation of the engine in C++, in old times that only the oldest French-speaking Internet users among you could have known.

The development of the game had begun, and had been the subject of several articles on the blog. A release date was even [announced at that time](/fr/news/2006-04-01-date-de-sortie-de-zelda-mercuris-chest): April 26th, 2023.

It became a running gag when we were asked about the release date of a Solarus project. Last year, we parodied the announcement of the postponement of _Tears of the Kingdom_ by announcing [the postponement of Mercuris' Chest for 2033](/fr/news/2022-04-01-mercuris-chest-report)!

![Screenshot from last year's video](zmc-report-screenshot.jpg)

## Time flies so fast…

Let's go back to 2006. Christopho [started the design](/fr/news/2008-04-01-annonce-officielle-zelda-mystery-of-solarus-dx) of a new engine in C++ named Solarus, then the redesign of _Mystery of Solarus_ with this new engine... Then came the creation of other games: _[Return of the Hylian](/fr/games/the-legend-of-zelda-return-of-the-hylian-se)_, _[Zelda XD2](the-legend-of-zelda-xd2-mercuris-chess)_... The development of the engine itself was time-consuming... and gradually, _Mercuris' Chest_ sank into oblivion for many players.

But Solarus Team kept this game somewhere inside their heads.

Years passed, projects followed one another, and 2023 arrived much faster than we would have imagined.

![All our projects linked to Solarus](all-the-projects.webp)

## The resurrection of the project

As soon as the first minutes of the new year 2023 passed, Metallizer, one of the team members, thought:

> “What if this 21-year old joke becomes a reality?”

This is how, in the greatest industrial secrecy, the development of _Mercuris' Chest_ restarted.

You read it right: a demo of _Mercuris' Chest_ will be released in April 2023, as announced exactly 21 years ago! The most famous vaporware among the Zelda fangames is finally going to become a reality. No, you're not dreaming: **the demo release date for this long-awaited game is April 1st, 2023**.

{{< game-thumbnail "the-legend-of-zelda-mercuris-chest" >}}

## Trailer

We leave you without further delay with the most epic trailer, which should make you excited while waiting for the release in a few days!

{{< youtube "SYw8QJ7DW6E" >}}
