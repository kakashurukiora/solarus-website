---
date: '2023-03-30'
excerpt: Mercuris' Chest va avoir droit à une démo, comme précédemment annoncé en 2002.
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: "Date de sortie pour la démo de The Legend of Zelda: Mercuris' Chest"
---

## Une annonce qui date de 2002

Souvenez-vous, [en 2002](/fr/news/2002-07-23-zelda-solarus-2), l’équipe derrière _Zelda: Mystery of Solarus_ annonçait en fanfare son prochain jeu : _Mercuris’ Chest_.

Une démo était même sortie à l’occasion, faite avec [The Games Factory](https://www.clickteam.com/fr/the-games-factory-2) puis [Multimedia Fusion](https://www.clickteam.com/multimedia-fusion-2). C’était bien avant la création du moteur en C++, dans des temps immémoriaux que seuls les plus vieux internautes francophones d’entre vous pourraient avoir connu.

Le développement du jeu avait commencé, et avait fait plusieurs fois l’objet d’articles sur le blog. Une date de sortie avait même été [annoncée à l’époque](/fr/news/2006-04-01-date-de-sortie-de-zelda-mercuris-chest), pour le 26 avril 2023.

C’était devenu un running gag lorsque l’on nous demandait la date de sortie d’un projet Solarus. L’année dernière, nous avions d’ailleurs parodié l’annonce du report de _Tears of the Kingdom_ en annonçant le [report de _Mercuris’ Chest_ pour 2033](/fr/news/2022-04-01-mercuris-chest-report) !

![Capture d’écran de la vidéo de l’année dernière](zmc-report-screenshot.jpg)

## Le temps passe si vite…

Revenons en 2006. Christopho s’était alors [lancé](/fr/news/2008-04-01-annonce-officielle-zelda-mystery-of-solarus-dx) dans la conception d'un nouveau moteur en C++ sous le nom de Solarus, puis la refonte de _Mystery of Solarus_ avec son nouveau moteur… Puis vint la création d’autres jeux : _[Return of the Hylian](/fr/games/the-legend-of-zelda-return-of-the-hylian-se)_, _[Zelda XD2](the-legend-of-zelda-xd2-mercuris-chess)_... Le développement du moteur lui-même fut chronophage… et progressivement, _Mercuris’ Chest_ sombra dans l’oubli pour de nombreux joueurs.

Mais la Team Solarus gardait ce jeu dans un coin de son esprit.

Les années passèrent, les projets s’enchaînèrent, et 2023 arriva beaucoup plus vite que nous ne l’aurions imaginé.

![Tous nos projets liés à Solarus](all-the-projects.webp)

## La résurrection du projet

Aussitôt les premières minutes de la nouvelle année 2023 écoulées que Metallizer, l’un des membres de l’équipe, se dit :

> “Et si cette blague datant d’il y a 21 ans devenait une réalité ?”

C’est ainsi que, dans le plus grand secret industriel, le développement de _Mercuris’ Chest_ reprit.

Vous l’avez bien lu : une démo de _Mercuris’ Chest_ va sortir en avril 2023, comme annoncé il y a exactement 21 ans ! Le vaporware le plus célèbre des fangames Zelda va finalement devenir une réalité. Non, vous ne rêvez pas : **la date de sortie de la démo de ce jeu tant attendu est le 1er avril 2023**.

{{< game-thumbnail "the-legend-of-zelda-mercuris-chest" >}}

## Bande-annonce

Nous vous laissons sans plus attendre avec une bande-annonce des plus épiques, qui devrait vous mettre l’eau à la bouche en attendant la sortie dans quelques jours !

{{< youtube "SYw8QJ7DW6E" >}}
