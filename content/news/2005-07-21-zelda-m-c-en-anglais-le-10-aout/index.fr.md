---
date: '2005-07-21'
excerpt: 'De nombreux joueurs anglophones nous réclament une version anglaise de Zelda : Mystery of Solarus. Malheureusement, le jeu n''avait pas été prévu...'
tags:
- solarus
title: 'Zelda : M''C en anglais le 10 août !'
---

De nombreux joueurs anglophones nous réclament une version anglaise de Zelda : Mystery of Solarus. Malheureusement, le jeu n'avait pas été prévu au départ pour être traduit un jour et ça demanderait maintenant trop de travail.

En revanche, Zelda : Mercuris' Chest va être traduit. Et c'est notre cher Géomaster qui se chargera de cette traduction. Je suis donc heureux de vous annoncer que la démo 2.0 en version anglaise sortira le 10 août, parlez-en autour de vous !

Géomaster n'est pas le premier venu en matière de traduction puisqu'il a traduit de l'anglais vers le français les mangas de Ocarina of Time, Oracle of Ages et Oracle of Seasons sur Zelda Solarus. Il a déjà également traduit en anglais la démo d'un Zelda amateur : il s'agit de [Zelda : Attack of the Shadow](http://zaots.free.fr), le jeu de Doc (un peu de pub ne fait pas de mal !).

Par ailleurs, le [mode d'emploi](http://www.zelda-solarus.com/jeux.php?jeu=zmc&zone=notice) de la démo vient d'être mis à jour pour prendre en compte les nouveautés de la version 2.0. Ce mode d'emploi sera également disponible en version anglaise pour le 10 août.
