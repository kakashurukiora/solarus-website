---
date: '2006-04-01'
excerpt: "La fête est terminée ! :mrgreen: J'espère que vous avez tous apprécié notre poisson d'avril, ou plutôt notre poisson d'avril géant..."
tags:
  - solarus
title: C'est déjà fini !
---

La fête est terminée ! :mrgreen:

J'espère que vous avez tous apprécié notre poisson d'avril, ou plutôt notre poisson d'avril géant découpé en 13 épisodes :D
Nous avons beaucoup rigolé en le préparant et en lisant vos réactions. Tout au long de la journée, vous avez été extrêmement nombreux à réagir. Nous étions loin de nous attendre à un tel succès !

L'idée de ce poisson d'avril est née seulement la nuit dernière, à 1h du matin. J'ai passé la nuit à écrire le scénario de l'histoire, jusqu'à 5h30 environ. Bien sûr, tout était absolument faux. Rassurez-vous, Couet n'a jamais été enlevée, Geomaster et Metallizer sont bien deux personnes différentes et ils sont des grands fans de Zelda, et le site n'a pas été piraté. Cependant, le serveur a vraiment eu des soucis à un moment donné. Il a été saturé à cause du trop grand nombre de visiteurs sur le forum. Super Tomate n'y était pour rien ^\_^

Un grand merci à tous ceux qui ont participé à la conception du poisson d'avril : merci à Geomaster et à Couet pour m'avoir aidé à la rédaction des fausses mises à jour tout au long de la journée, merci à Metallizer, merci aux modérateurs qui ont eu la permission exceptionnelle de flooder, avec les comptes légumes (parfois un peu trop d'ailleurs :D) et merci à @PyroNet pour avoir imaginé et conçu le design spécial Sony en une après-midi.

**UPDATE :** le record du nombre de pages vues du site a été largement battu grâce au poisson d'avril. Selon Xiti, il y a eu plus de 57 000 pages vues au cours de la journée !

**UPDATE 2 :** pour ceux qui ont manqué le poisson d'avril, vous ne comprendrez rien si vous ne lisez pas les dernières news dans l'ordre, en commençant par celle de 11h du matin :P
