---
date: '2002-04-26'
excerpt: Salut à tous ! D'abord je vous dis un grand merci car vous êtes très nombreux à avoir déjà téléchargé le jeu ! Ce soir à 20 heures (heure...
tags:
- solarus
title: Grand chat ce soir !
---

Salut à tous !

D'abord je vous dis un grand merci car vous êtes très nombreux à avoir déjà téléchargé le jeu !

Ce soir à 20 heures (heure française), nous organisons un grand chat pour parler de Zelda Solarus ! Vous pouvez accéder au chat en cliquant sur le lien du menu de gauche.

Soyez nombreux ce soir !
