---
date: '2010-09-22'
excerpt: I have just switched the source code from Subversion to Git. The code can now be accessed on github, which provides a nicer interface for the history...
tags:
- solarus
title: Solarus moves to Git and GitHub
---

I have just switched the source code from Subversion to [Git](http://github.com). The code can now be accessed on [github](http://github.com/christopho/solarus), which provides a nicer interface for the history of commits and many more features.

Your svn local copy should be replaced by a local fork of the git repository. Everything is explained on the [Solarus github](http://github.com/christopho/solarus) page, but to be short, use the following command to download the code (obviously, git needs to be installed):

```
git clone git://github.com/christopho/solarus.git
```

You now have a local fork of the repository, including the full history of commits. You may modify the code and even do your own local commits since git is a distributed versioning system. Then, if you want me to include your modifications into the official repository, you can make a request with git. Feel free to contribute!

Note: the SVN repository is still available but is now obsolete. New commits are only made on the Git repository. The SVN repository remains at revision 1443.
