---
date: '2015-05-09'
excerpt: A bugfix version of Solarus was just released! This is version 1.4.1 1.4.2. It fixes some important problems including a crash with doors or chest...
tags:
- solarus
title: Bugfix release 1.4.2
---

A bugfix version of Solarus was just released!

This is version ~~1.4.1~~ 1.4.2. It fixes some important problems including a crash with doors or chest using an equipment item as opening condition, and some problems in the quest editor when resizing maps or when copy-pasting entities. Thanks for your feedback!

- [Download Solarus 1.4](http://www.solarus-games.org/engine/download/ "Download Solarus")
- [Solarus Quest Editor](http://www.solarus-games.org/engine/solarus-quest-editor/)
- [Lua API documentation](http://www.solarus-games.org/doc/latest/lua_api.html "LUA API documentation")
- [Migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide)

## And as always, here is the full changelog. Changes in Solarus 1.4.2

- Fix crash with doors whose opening condition is an item (#686).
- Fix the size of custom entities supposed to be optional (#680).
- Fix the hero's sprite reset to default ones when changing equipment (#681).
- Fix animated tiles freezed when running a quest a second time (#679).
- Fix saving empty files.
- Print an error message when there is no font in the quest.

## Changes in Solarus Quest Editor 1.4.2

- Fix crash when copy-pasting both tiles and dynamic entities sometimes (#24).
- Fix crash when creating a map when no tileset exists.
- Fix the view not updated when resizing a map (#25).
- Fix maps having having an initial size of zero.
- Fix inversion between generalized and usual NPCs in the entity dialog.
- Fix the choice of the layer when copy-pasting entities.
- Fix a precision issue when adding or moving entities.
- Fix entities remaining visible when adding them on a hidden layer.
- Fix the entity dialog allowing special characters in the entity name.
- Fix missing translation of special destination names.
- Fix closing a file even if unsaved when renaming it.
- Fix typos.
- Sprite editor: fix adding a new direction with the plus button (#27).

Enjoy!
