---
date: '2017-12-08'
excerpt: Alors que récemment nous vous parlions de la grande ville qui petit à petit se remplit de nombreux personnages, aujourd'hui nous allons nous...
tags:
  - solarus
title: Des personnages inédits
---

Alors que récemment nous vous parlions de la grande ville qui petit à petit se remplit de nombreux personnages, aujourd'hui nous allons nous intéresser à ce qu'il se passe dans une des maisons qui composent cette cité.

**Mercuris' Chest** possèdera son lot de personnages inédits et spécifiquement conçus pour ce nouvel opus. Cependant nous ne dévoilerons ni l'identité de ce personnage ni ses motivations, à vous de le découvrir lors de votre aventure. ;)

![](interieurmaisonMC-300x225.png)

À bientôt sur Zelda Solarus pour de prochaines nouvelles.
