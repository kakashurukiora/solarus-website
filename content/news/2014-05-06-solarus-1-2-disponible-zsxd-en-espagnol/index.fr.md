---
date: '2014-05-06'
excerpt: Après plusieurs mois de développement, et malgré un temps significatif passé à jouer à 2048, la nouvelle version du moteur Solarus est...
tags:
- solarus
title: Solarus 1.2 disponible, ZSXD en espagnol
---

Après plusieurs mois de développement, et malgré un temps significatif passé à jouer à 2048, la nouvelle version du moteur Solarus est terminée !

Le principal changement visible pour le joueur est que les jeux tirent maintenant profit de l'accélération graphique de votre carte vidéo. Le jeu devrait être plus fluide sur les machines peu puissantes. La fenêtre est maintenant redimensionnable, le passage en plein écran se fait de façon fluide sans changer la résolution. De nouveaux types de filtres de lissage de pixels sont disponibles, ce qui rend le résultat de meilleure qualité sur les grands écrans d'aujourd'hui pour ceux qui n'aiment pas les gros pixels.

Si vous utilisez Solarus pour développer votre propre jeu, il y a énormément de nouveautés. L'éditeur de quêtes a été largement amélioré : on peut redimensionner plusieurs entités en même temps, le copier-coller est plus intuitif et on peut déplacer la vue et zoomer avec le bouton du milieu de la souris. Un nouveau type d'entité est disponible : les entités personnalisées, qui sont entièrement scriptées. En outre, on peut désormais personnaliser les objets destructibles (pots, buissons) ainsi que les tapis roulants. Bref on repousse les limites !

- Télécharger [Solarus 1.2 + l'éditeur de quêtes](http://www.solarus-games.org/downloads/solarus/win32/solarus-1.2.0-win32.zip) pour Windows
- Télécharger le [code source](http://www.solarus-games.org/downloads/solarus/solarus-1.2.0-src.tar.gz)
- Liste complète des [changements](https://github.com/christopho/solarus/blob/v1.2.0/ChangeLog)
- [Blog de développement Solarus](http://www.solarus-games.org/)
- Comment [convertir votre quête](http://wiki.solarus-games.org/doku.php?id=fr:migration_guide) de Solarus 1.1 vers Solarus 1.2 (un tuto vidéo sera bientôt disponible)

Du côté de nos créations, comme toujours elles ont été mises à jour pour profiter de ces améliorations et de corrections de bugs. L'occasion de finir ces jeux si vous ne l'avez pas encore fait !

- Télécharger [Zelda Mystery of Solarus DX 1.8](http://www.zelda-solarus.com/zs/article/zmosdx-telechargements/)
- Télécharger [Zelda Mystery of Solarus XD 1.8](http://www.zelda-solarus.com/zs/article/zmosxd-telechargements/)

À noter que ZSXD est maintenant disponible en espagnol. Merci Xexio !

Beaucoup de changements ont été apportés au moteur et il est possible que des bugs soient apparus. N'hésitez pas à les signaler si vous en trouvez !
