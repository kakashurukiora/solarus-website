---
date: '2003-04-01'
excerpt: 'Petit rappel pour ceux qui débarquent : "ZAP" ce sont les initiales du titre provisoire de notre prochaine création : Zelda Advanced Project. Ce...'
tags:
- solarus
title: 'ZAP : L''avancement de la démo'
---

Petit rappel pour ceux qui débarquent : "ZAP" ce sont les initiales du titre provisoire de notre prochaine création : Zelda Advanced Project. Ce titre n'évoque rien sur le scénario pour la simple raison que ce dernier est encore en cours d'élaboration. Nous n'avons même pas encore trouvé le titre définitif en fait.

La démo ne contiendra de toute façon aucun scénario. Il s'agit simplement d'une démo technique qui nous l'espérons vous étonnera par rapport à Zelda Solarus du point de vue de la programmation. Cette démo, que nous développons avec le logiciel The Games Factory, ne contiendra rien d'autre qu'un donjon. Mais c'est un donjon inédit, qui ne sera à priori pas dans le jeu complet. Nous l'avons conçu pour être long et anti-linéaire.

Quoi de neuf depuis ces derniers jours ? Et bien le moteur de jeu est presque au point, nous avons enfin réussi à faire marcher le système de combats en corrigeant un bug qui nous embêtait depuis des semaines. Les objets auxquels vous aurez droit dans ce donjon sont également programmés, dont un qui nous avait posé pas mal de problèmes dans Zelda Solarus :-)

Tout cela pour dire que même si la date de sortie de la démo n'est toujours pas annoncée, ça avance bien. Le moteur de jeu étant désormais presque terminé ça ira maintenant beaucoup plus vite. Nous avons une date de sortie en tête mais nous la communiquerons au dernier moment afin d'être sûrs de la respecter. En effet on peut toujours avoir de mauvaises surprises : rappelez-vous, nous pensions au départ sortir la démo à Noël ! Alors nous préférons ne rien annoncer comme date pour éviter tout retard.
