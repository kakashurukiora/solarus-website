---
date: '2007-04-01'
excerpt: "...a fait déborder le vase? Je ne pourrai pas vous donner les news promises sur Mercuris' Chest. Malheureusement, suite aux nombreuses critiques..."
tags:
  - solarus
title: La goutte d'eau...
---

...a fait déborder le vase?
Je ne pourrai pas vous donner les news promises sur Mercuris' Chest. Malheureusement, suite aux nombreuses critiques sur le nouveau design du site et du forum, et sur la qualité des news de Marco, j'ai été contraint de mettre fin au projet.
Et en voici la preuve :

![](http://www.zelda-solarus.com/images/actu/barbecue.jpg)

Le projet Mercuris' Chest est donc annulé et détruit. Les critiques auront eu raison du projet :mrgreen:

Notez aussi que l'équipe de modération du forum a été remplacée par une équipe toute fraîche, une armée de Zora qui est là pour sauver ce qui peut encore l'être :)
