---
date: '2003-01-26'
excerpt: Salut à tous ! Les problèmes de connexion au chat semblent avoir été réglés et tout le monde a pu participer au quizz ! Les scores on été...
tags:
- solarus
title: Résultats du concours !
---

Salut à tous !

Les problèmes de connexion au chat semblent avoir été réglés et tout le monde a pu participer au quizz ! Les scores on été très serrés et tout s'est joué sur les dernières questions. Voici le classement définitif :

- **1er** : flobo avec 22 points
- **2e** : Anthony Stabile avec 21 points
- **3e** : Link-Sheik avec 12 points
- **4e** : snake la terreur avec 9 points
- **5e** : Benny avec 8 points
- **5e ex-aequo** : Oxyd avec 8 points
- **7e** : Kafei avec 6 points
- **8e** : billy bob avec 5 points
- **9e** : leed avec 3 points
- **10e** : Djeepy46234 avec 1 point
- **10e ex-aequo** : S-Premier avec 1 point
- **10e ex-aequo** : Jayman avec 1 point
- **10e ex-aequo** : aurelien1234 avec 1 point

Voilà, on espère que vous vous êtes bien amusés ! Bravo à flobo qui recevra la démo de ZAP en avant-première quand elle sera finie, et bravo à mega bowser et à Link-Sheik qui recevront quant à eux le CD des musiques de ZS. Et merci à tous d'avoir participé !
