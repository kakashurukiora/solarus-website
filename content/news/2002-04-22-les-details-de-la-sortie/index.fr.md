---
date: '2002-04-22'
excerpt: Jour J-3 ! Voici probablement la dernière mise à jour avant le Jour J. Tout est prêt, il ne me reste plus qu'à compiler le jeu et à créer le...
tags:
- solarus
title: Les détails de la sortie
---

Jour J-3 !

Voici probablement la dernière mise à jour avant le Jour J. Tout est prêt, il ne me reste plus qu'à compiler le jeu et à créer le programme d'installation. J'attends encore jusqu'à demain, afin d'avoir le temps de corriger les derniers bugs que certains des testeurs arrivent à trouver.

Il faut quand même que je vous dise que Netgamer est parti en vacances pour la semaine. Il ne sera donc pas là pour la sortie du jeu mais ne vous inquiétez pas, j'étais prévenu et il avait fait l'essentiel de ce qui lui restait à faire avant de partir.

Je vais maintenant vous expliquer ce que vous aurez à télécharger.

Zelda Solarus utilise certains fichiers de RTP de RPG Maker. Mais heureusement, contrairement à la démo, ces fichiers sont inclus dans le programme d'installation général. Vous n'aurez donc pas besoin de télécharger les 11.7 Mo de RTPe.exe. De plus, ZS n'utilise pas tous les RTP (seulement certains effets sonores, certains persos et certains boss), donc j'ai pris soin de les trier pour que vous n'ayez que le minimum à télécharger. Et Netgamer a même modifié les sons en les enregistrant au format 8 bits au lieu de 16 pour diminuer leur taille presque par 2. Et on n'entend pas de différence de qualité sonore.

Cependant, même si ces fichiers ne font donc qu'au total approximativement 2 à 3 Mo compressés (vous connaîtrez le chiffre exact vendredi), ce serait quand même du téléchargement inutile pour ceux qui ont déjà les RTP installés sur leur ordinateur. Nous vous proposerons donc deux packages :

- La version normale, pour ceux qui n'ont pas les RTP. Elle devrait faire environ 6 à 7 Mo.
- La version allégée, pour ceux qui ont déjà les RTP. Cette version n'installe pas les fichiers provenant des RTP puisque vous les avez déjà. Taille : 3.8 Mo.

Dans tous les cas, vous aurez un seul fichier à télécharger (le programme d'installation). Vous n'aurez ensuite qu'à le lancer et à suivre les instructions à l'écran. Et tout sera installé : le jeu, les polices d'écran, le fichier d'aide...

Le jeu sera disponible vendredi vers 13 h 30. La date du 26 avril est fixée depuis le 5 février et le jeu n'aura donc pas été retardé malgré quelques événements imprévus. Vous décrouvrirez également, en même temps que le jeu, la grosse surprise que nous vous avions promise il y a quelques semaines...
