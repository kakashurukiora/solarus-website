---
date: '2017-11-27'
excerpt: Chères Solarussiennes, chers Solarussiens, il y a longtemps que nous n'avions pas donné de nouvelles de notre prochaine aventure en cours de...
tags:
- solarus
title: 'Mercuris'' Chest : la grande ville se remplit'
---

Chères Solarussiennes, chers Solarussiens, il y a longtemps que nous n'avions pas donné de nouvelles de notre prochaine aventure en cours de création, qui, depuis de nombreuses années, vous met l'eau à la bouche : **Mercuris' Chest**.

Chose réparée aujourd'hui, avec de nouvelles captures d'écran, où la grande ville se remplit de PNJ qui auront de nombreuses choses à raconter à Link, lui proposant moult quêtes et défis en tout genre qui sauront agrémenter la quête principale comme dans tout bon Zelda qui se respecte.
![](screen2-300x227.png)

![](town-300x225.png)

![](town2-300x225.png)

Comme vous pouvez le constater, ça avance tranquillement mais sûrement, et la Solarus Team continue son travail pour enfin un jour vous proposer Mercuris' Chest en version complète (2023 ?).
