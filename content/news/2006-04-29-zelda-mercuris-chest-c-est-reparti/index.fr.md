---
date: '2006-04-29'
excerpt: J'avais annoncé dans une  que le développement de notre projet serait en pause.
tags:
  - solarus
title: "Zelda Mercuris' Chest :  c'est reparti !!!"
---

J'avais annoncé dans une [précédente mise à jour](http://www.zelda-solarus.com/index.php?id=514) que le développement de notre projet Zelda : Mercuris' Chest serait en pause jusqu'au 21 avril à cause de mes études. Nous sommes aujourd'hui le 29 avril et je suis heureux de vous annoncer que le développement a effectivement repris depuis quelques jours. ^\_^

J'ai travaillé ces derniers jours sur la scène de l'intérieur de la maison dans laquelle dort paisiblement Link dans l'intro. Ne comptez pas sur moi pour vous en dire plus sur l'intro. :P

Une capture d'écran de cette maison sera probablement bientôt publiée. Mais en attendant, vous pouvez continuer à faire des hypothèses par vous-mêmes sur l'intro, notamment à l'aide de la capture d'écran [dévoilée à Noël](http://www.zelda-solarus.com/index.php?id=495). Cette image est également tirée de l'intro et les plus malins peuvent en déduire des informations précieuses sur le scénario tout entier... B)
