---
date: '2011-03-31'
excerpt: "Bonsoir à tous ! Il y a longtemps que vous n'avez pas eu de nouvelles concernant notre remake de Zelda : Mystery of Solarus, et pour cause, son..."
tags:
  - solarus
title: Zelda Mystery of Solarus XD, le jeu complet
---

Bonsoir à tous !

Il y a longtemps que vous n'avez pas eu de nouvelles concernant notre remake de Zelda : Mystery of Solarus, et pour cause, son avancée à été plus que fulgurante, tellement rapide que nous sommes parvenus à concrétiser notre projet, et à le mener à son terme !
Nous vous présentons donc **« The Legend of Zelda : Mystery of Solarus XD »** !

![](http://www.zelda-solarus.com/images/zsxd/affiche_550.jpg)

Quoi ? Vous attendiez autre chose ?
Ah, chers amis, il vous faut savoir apprécier ce que nous vous mettons en avant !

- Télécharger [Zelda: Mystery of Solarus XD](http://www.zelda-solarus.com/jeu-zsxd-download)

Voilà le deal : finissez (ou du moins tentez de finir) cette aventure. Après cela, vous ne verrez plus Link comme avant, et vous serez fin prêts pour les prochaines aventures de notre Hylien préféré. À vaincre sans péril, on triomphe sans gloire, comme on dit, n'est-ce pas ?
Allez, puisse son chemin mener le héros à la Forcetri, et une dernière chose : Zelda a disparu, enfin comme d'habitude quoi !

Bonne chance à tous, vous en aurez besoin sur ce coup là !
