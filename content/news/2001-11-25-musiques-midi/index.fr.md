---
date: '2001-11-25'
excerpt: Je serai bref en vous annonçant que vous pouvez écouter et télécharger les musiques du jeu au format MIDI dans la section prévue à cet effet...
tags:
- solarus
title: Musiques MIDI
---

Je serai bref en vous annonçant que vous pouvez écouter et télécharger les musiques du jeu au format MIDI dans la section prévue à cet effet !

[Accéder aux musiques MIDI](http://www.zelda-solarus.com/midi.php3)
