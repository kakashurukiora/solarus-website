---
date: '2019-08-20'
excerpt: La dernière version de Solarus est intégrée à Retropie.
tags:
- solarus
thumbnail: cover.jpg
title: Solarus dans RetroPie
---

La dernière version de Solarus est désormais officiellement intégrée dans **RetroPie** !

Solarus a été promu comme *système* dans une récente mise à jour de RetroPie, exactement comme d'autres moteurs de jeux populaires comme Löve, Adventure Game Studio (AGS) et ScummVM.

De plus, il a été mis à jour à la toute dernière vesrion, ce qui permet de jouer à plus de quêtes qu'auparavant dans RetroPie.

## Qu'est ce que RetroPie ?

![Logo de Retropie](retropie-logo.png)

Tiré du [site officiel](https://retropie.org.uk/) de RetroPie (et traduit):

> RetroPie permet à votre Raspberry Pi ou votre PC de se transformer en machine de rétro-gaming.
>
> Il est basé sur Raspbian, EmulationStation, RetroArch et d'autres projets pour vous permettre de jouer à vos jeux arcade, console ou PC en un minimum d'étapes d'installtion.
>
> RetroPie repose sur un OS complet, vous pouvez l'installer sur un Raspbian déjà existant, ou démarrer depuis zéro avec l'image de RetroPie SD et installer d'autres logiciels après. C'est comme vous voulez.

Dans RetroPie, les ordinateurs et consoles émulées sont organisés en *systèmes*, chacun associés à une collection de jeux (ou *roms*).

Cela permet une gestion des jeux et une navigation très simples, par *système*, en utilisant l'interface par défaut d'EmulationStation :

![EmulationStation dans RetroPie](retropie-es.png)

En utilisant cette approche, les utilisateurs ont juste besoin de placer leurs jeux/roms dans des dossiers correspondant à chaque *système*, recharger EmulationStation et le contenu ajouté sera alors visible et prêt à être lancé depuis les menus.

Puisque la plupart des quêtes Solarus sont distribuées en paquets `.solarus` actuellement et que le moteur de jeu Solarus peut être envisagé comme un *émulateur* pour ces paquets, faire de Solarus un *système* dans RetroPie était naturel !

Plus important, cette amélioration permet aux créateurs de quêtes Solarus de distribuer leurs quêtes plus facilement aux utilisateurs RetroPie.

## Solarus dans RetroPie

Solarus faisait déjà partie de RetroPie depuis la version `1.4.5` (ajouté en 2016), avec trois quêtes installées à côté :

- The Legend of Zelda: Mystery of Solarus DX
- The Legend of Zelda: Mystery of Solarus XD
- The Legend of Zelda: Return of the Hylian SE

Cependant ces quêtes étaient intégrés comme *ports* dans RetroPie, ce qui signifie que pour ajouter des nouvelles quêtes, il fallait créer des scripts de lancement dédiés pour chaque quête. Bien entendu, ce n'était pas pratique.

De plus, Solarus dans RetroPie n'avait pas été mis à jour depuis longtemps, alors que les nouvelles versions apportaient des améliorations très importantes comme l'accélération matérielle du renderer OpenGL.

Pour ces raisons, trois objectifs principaux pour Solarus dans RetroPie avaient été fixés :

- Mettre une version à jour de Solarus dans RetroPie (version actuelle `1.6.x`)
- Intégrer Solarus comme *système* pour que les quêtes soient lancées directement depuis un dossier dédié
- Optimiser l'utilisation de Solarus pour du jeu dans le salon (pas de clavier nécessaire)

À compter d'aujourd'hui et grâce à quelques mois de travail d'Hugo Hromic, la Solarus Team est fière de vous annoncer que tous ces objectifs ont été atteints !

### Solarus comme *système*

Solarus est désormais un citoyen de première classe dans le menu principal d'EmulationStation, aux côtés des autres émulateurs.

![Screenshot](screenshot-01.png)

Des ressources pour le thème Carbon (par défaut dans RetroPie) ont été fournies, ainsi que pour le thème Simple (modes sombre et foncé). Tout cela est déjà pré-installé par défaut.

### Dossier dédié pour les quêtes

Retropie n'installe plus de quêtes Solarus avec le moteur. À la place, les utilisateurs doivent simplement télécharger leurs quêtes Solarus préférées (au format `.solarus`) et les placer dans un dossier Solarus dédié : `~/RetroPie/roms/solarus`. Après avoir redémarré EmulationStation (afin de recharger les listes de jeux), toutes les quêtes téléchargées sont disponibles et prêtes à être jouées d'un simple clic.

En d'autres termes, les créateurs de quêtes Solarus peuvent désormais distribuer leurs quêtes aux utilisateurs RetroPie !

![Screenshot](screenshot-02.png)

### Metadonnées de quêtes et médias

Une des plus célèbres fonctionnalités d'EmulationStation est la capacité d'afficher des métadonnées (telles que l'éditeur, la description, la note, etc.) et des images (telles que la pochette ou des captures d'écran) correspondant aux jeux, pour une expérience de navigation accrue. Bien que Solarus ne soit pas encore disponible sur aucun *scraper* en ligne, cela n'enlève rien à la possibilité de s'amuser à ajouter les metadonnées et les images à la main !

![Screenshot](screenshot-03.png)

## Utiliser Solarus dans RetroPie

Si RetroPie n'est pas déjà installé sur votre machine, vous pouvez sur les [instructions d'installation officielles](https://retropie.org.uk/docs/First-Installation/) (en anglais).

Pour utiliser Solarus sur votre installation RetroPie, assurez vous d'utiliser la dernière version du script d'installation de RetroPie en utilisant l'option *Update* dans le menu principal de RetroPie.

Ensuite, dans le menu principal, choisissez *Manage Packages* puis *Manage optional packages*. Ici, vous pourrez trouver le paquet *solarus* pour l'installer.

Pour obtenir la toute dernière version de Solarus, vous devez utiliser l'option *Update from source*, cependant la compilation peut prendre un certain temps. Si vous ne souhaitez pas attendre, vous pouvez utiliser l'option *Update from binary* pour obtenir un binaire pré-compilé préparé régulièrement par les mainteneurs RetroPie (environ mensuellement).

Après avoir installé Solarus depuis le menu principal de RetroPie, vous êtes prêts à télécharger des quêtes dans le dossier de Solarus dédié *rom* (`~/RetroPie/roms/solarus`). Plus de précisions sur la transfert de jeux vers votre machine sont disponibles dans la [documentation officielle RetroPie](https://retropie.org.uk/docs/Transferring-Roms/).

### Configurer Solarus dans RetroPie

La nouvelle intégration RetroPie de Solarus inclut une interface utilisateur (UI) pour configurer votre installation.

Elle est accessible depuis le menu principal de RetroPie en choisissans l'option *Configuration / tools* puis l'option *solarus*.

Actuellement, il y a deux configurations d'options basiques pour Solarus dans RetroPie.

- **Set joypad axis deadzone :** pour choisir la sensibilité du stick analogique dans Solarus
- **Set joypad quit buttons combo :** pour choisir le combo de boutons pour quitter Solarus en plein jeu

Ces options sont conçues pour améliorer l'utilisation de Solarus lors du *couch-playing* (sans clavier).

Pour davantage d'information sur RetroPie et pour obtenir de l'aide sur l'intégration de Solarus, visitez leurs [forums officiels](http://retropie.org.uk/forum/).

Nous espérons que les créateurs de quêtes et les joueurs pourront tous apprécier l'exprérience de jouer à Solarus sur grand écran dans leurs salons avec RetroPie !
