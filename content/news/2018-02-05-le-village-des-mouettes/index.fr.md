---
date: '2018-02-05'
excerpt: Comme chaque semaine, nous continuons à vous dévoiler de nouvelles images de notre jeu, remake de Link's Awakening. Cette semaine, c'est le village...
tags:
  - solarus
title: Le village des Mouettes
---

Comme chaque semaine, nous continuons à vous dévoiler de nouvelles images de notre jeu, remake de Link's Awakening. Cette semaine, c'est le village des Mouettes qui est à l'honneur ! Je suis certain que vous n'aurez pas de mal à reconnaitre les différents lieux du village sur ces images.

![](1-300x240.png)

![](2-300x240.png)

![](3-300x240.png)

![](4-300x240.png)
