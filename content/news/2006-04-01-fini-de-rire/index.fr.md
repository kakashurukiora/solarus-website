---
date: '2006-04-01'
excerpt: Je n'étais pas censé le dévoiler, mais tôt ou tard Christopho aurait tout découvert. J'ai infiltré l'équipe du site il y a 5 ans, et j'ai...
tags:
- solarus
title: Fini de rire !
---

Je n'étais pas censé le dévoiler, mais tôt ou tard Christopho aurait tout découvert. J'ai infiltré l'équipe du site il y a 5 ans, et j'ai travaillé pour Christopho afin d'acquérir petit à petit sa confiance. D'abord sous le pseudo de Netgamer, puis de Metallizer, mais ça s'est mal fini. Mais bien décidé à ne pas laisser mes plans tomber à l'eau, je suis revenu avec une nouvelle identité : Geomaster.

Cette imbécile de Super Tomate devait m'aider à assouvir mes desseins mais comme tout larbin, sa faiblesse a failli ruiner mes plans. J'aurais dû dès le départ prendre les choses en main mais j'avais ma couverture à préserver.

Maintenant je suis le seul maître à bord et je vais pouvoir accomplir la destinée finale de Zelda Solarus !

Je n'aime pas Zelda. Je n'aime pas Nintendo et leur politique de l'innovation. L'innovation ne mènera jamais le monde à la prospérité alors que la technologie et le progrès, oui ! Nintendo doit être éradiqué afin que la civilisation puisse reprendre la course normale de son évolution !

Cette sombre idiote de Super Tomate et son potager ambulant n'ont pas réussi la mission que je leur avais confiée. Mais qu'importe, dorénavant, Zelda Solarus devient un site entièrement consacré à Sony et à ses merveilleuses consoles : la PSOne, la PS2, la PSP et la future PS3 qui marquera une fois pour toutes la victoire de Sony et son avènement dans le monde entier !

Comme vous avez déjà pu le constater, j'ai fait quelques modifications rapides au design du site afin de refléter cette ère nouvelle qui approche. D'autres actions, plus fortes, auront lieues un peu plus tard mais vous découvrirez vous-mêmes de quoi il s'agit.
