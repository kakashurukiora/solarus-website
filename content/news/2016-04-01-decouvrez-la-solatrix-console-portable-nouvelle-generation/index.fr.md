---
date: '2016-04-01'
excerpt: Depuis plusieurs années, et au risque de sacrifier la fréquence des mises à jour, nous développions dans le plus grand secret un projet d'une...
tags:
- solarus
title: Découvrez la Solatrix, console portable nouvelle génération
---

Depuis plusieurs années, et au risque de sacrifier la fréquence des mises à jour, nous développions dans le plus grand secret un projet d'une ampleur inédite.

Cette fois il ne s'agit pas d'un jeu, ni d'un moteur de jeu, ni d'un logiciel de création de jeux. Nous sommes allés plus loin.

L'équipe est fière de vous dévoiler officiellement la Solatrix, une console portable révolutionnaire dédiée aux jeux utilisant le moteur Solarus !

![logo_solatrix](logo_solatrix-300x65.png)

La Solatrix vous permettra de télécharger les jeux créés avec Solarus Quest Editor, aussi bien ceux de notre équipe que les jeux de la communauté. Il est prévu qu'elle soit disponible pour les fêtes, même s'il reste à déterminer de quelle année.

![La Solatrix](console1-300x225.jpg)

Légère et maniable, la Solatrix est une console portable de nouvelle génération connectée, et dotée d'un écran 3,5 pouces rétroéclairé.
Pour les créateurs de jeux, Solarus Quest Editor proposera l'export direct de votre quête vers la Solatrix :

![Export depuis Solarus Quest Editor vers la Solatrix](editor_screenshot-277x300.png)

Le Pack Mercuris vous proposera la console accompagnée de Zelda Mercuris' Chest, notre vaporware phare.

![console2](console2-225x300.jpg)

Compatible avec les manettes les plus populaires du marché, la Solatrix vous promet des moments de jeu plus intenses que jamais.

![console3](console3-300x225.jpg)
Toute l'équipe de Solarus Software a travaillé d'arrache-pied pour donner vie à ce projet ! Les plans seront bientôt disponibles afin que vous puissiez vous aussi construire votre Solatrix.
![console4](console4-300x225.jpg)

On se retrouve dès l'année prochaine pour une nouvelle mise à jour. Voilà en tout cas de belles heures de jeu et de joie en perspective !
