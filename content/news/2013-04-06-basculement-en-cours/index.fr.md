---
date: '2013-04-06'
excerpt: "Peu à peu, nous sommes en train de transférer tout le contenu de l'ancienne version du site vers la nouvelle. \tLes téléchargements de nos jeux..."
tags:
- solarus
title: Basculement en cours
---

Peu à peu, nous sommes en train de transférer tout le contenu de l'ancienne version du site vers la nouvelle.

- Les [téléchargements de nos jeux](http://www.zelda-solarus.com/zs/telechargements/ "Téléchargements") sont désormais au complet (certains systèmes d'exploitations étaient manquants depuis l'ouverture du site).
- La solution de [Zelda Mystery of Solarus XD](http://www.zelda-solarus.com/zs/article/zmosxd-soluces/ "Soluce vidéo") est de retour, mieux organisée qu'avant grâce à Binbin. Elle sera prochainement complétée car la partie textuelle n'était pas encore exhaustive !
- Les solutions de [A Link to the Past](http://www.zelda-solarus.com/zs/article/zalttp-soluce/), [Ocarina of Time Master Quest](http://www.zelda-solarus.com/zs/article/zoot-soluce/) et [Majora's Mask](http://www.zelda-solarus.com/zs/article/zmm-soluce/) sont en cours de transfert (merci Renkineko, Valoo et Metallizer).

Il y a également du nouveau contenu disponible, comme [l'interview de Billy le Téméraire](http://www.zelda-solarus.com/zs/article/zmosdx-interview-de-billy-le-temeraire/ "Interview de Billy le Téméraire") à ne pas rater si vous avez joué à Zelda Mystery of Solarus DX. Il y a aussi de nouvelles galeries d'images, et d'autres sont en prévision de même les musiques originales de Zelda Mystery of Solarus DX. Et la solution de Link's Awakening, qui n'avait jamais été terminée jusqu'ici, va enfin être poursuivie grâce au travail de Morwenn !

De mon côté, j'avance dans l'ombre sur Mercuris' Chest. Je vous donnerai bientôt plus d'informations sur la reprise du projet !
