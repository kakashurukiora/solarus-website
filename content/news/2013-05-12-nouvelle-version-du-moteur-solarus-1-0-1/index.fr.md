---
date: '2013-05-12'
excerpt: ' Une mise à jour de notre moteur de jeu Solarus vient de sortir ! Au programme, une vingtaine de corrections de bugs et des améliorations...'
tags:
- solarus
title: 'Nouvelle version du moteur : Solarus 1.0.1'
---

![Logo du moteur Solarus](solarus-logo-black-on-transparent-300x90.png)

Une mise à jour de notre moteur de jeu Solarus vient de sortir !

Au programme, une vingtaine de corrections de bugs et des améliorations dans l'éditeur de quêtes. Les détails sont disponibles sur le [blog de développement](http://www.solarus-games.org/2013/05/12/solarus-1-0-1-released/). La principale amélioration de l'éditeur de maps est que vous pouvez maintenant afficher ou non chaque type d'entité (ennemis, coffres, tiles). Je ne peux que vous conseiller de passer à cette nouvelle version si vous utilisez Solarus pour développer un jeu :).

- Télécharger [Solarus 1.0.1 + l'éditeur de quêtes](http://www.solarus-games.org/downloads/solarus/win32/solarus-1.0.1-win32.zip) pour Windows
- Télécharger [Solarus 1.0.1 + l'éditeur de quêtes](http://www.solarus-games.org/downloads/solarus/macosx/solarus-1.0.1-macosx64.zip) pour Mac OS X
- Télécharger le [code source](http://www.solarus-games.org/downloads/solarus/solarus-1.0.1-src.tar.gz)

PS : la version Mac OS X est maintenant disponible ! Merci à Vlag :)
