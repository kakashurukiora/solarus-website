---
date: '2020-08-01'
excerpt: We often get asked about making a commercial game with Solarus. Here is some clarification.
tags:
- games
thumbnail: cover.jpg
title: Clarification about Solarus quest licensing
---

## Engine and Quest files licensing

Solarus engine is licensed under **GNU GPL v3**. Lots of quests made with the engine have been licensed the same, but it doesn't mean all quests need to be licensed under GPL. Actually, you can license your quest as you want, but you must follow some rules, depending on your licensing choice. It means that YES you can make a proprietary commercial game with Solarus.

A [chapter about licensing](/en/development/tutorials/solarus-official-guide/basics/choosing-a-license) has been added to the online tutorial book, and explains things more in detail.

![Solarus and Solarus quests licensing](solarus-licensing.png)

## The proprietary-commercial-friendly Solarus MIT Starter Pack

Coincidentally, Solarus community has initiated a project called [MIT Starter Quest](/en/development/resource-packs/mit-starter-quest). This ressource pack contains only MIT-licensed Lua scripts and CC0 (a.k.a. public domain) resources. It means you can use these scripts and resources for commercial proprietary projects. Some essential scripts like the one that handles multi-events or the dialog box have been re-written and licensed under MIT.

![MIT Starter Quest thumbnail](mit-starter-quest-thumbnail.png)
