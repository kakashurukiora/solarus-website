---
date: '2022-04-01'
excerpt: Nous avons une annonce à vous faire à propos de la date de sortie de ce jeu très attendu.
tags:
  - solarus
  - games
thumbnail: cover.png
title: "Date de sortie de The Legend of Zelda: Mercuris' Chest"
---

Le créateur de Solarus et producteur de _The Legend of Zelda: Mystery of Solarus_, Christopho, a une information à partager à propos de la date de sortie de _The Legend of #Zelda: Mercuris' Chest_. Jetez-y un coup d'œil.

{{< youtube "bHj5qEnq0fA" >}}

{{< game-thumbnail "the-legend-of-zelda-mercuris-chest" >}}
