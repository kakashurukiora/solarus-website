---
date: '2002-03-18'
excerpt: Salut à tous ! J'ai refait les chipsets de la carte du monde, car les anciens graphismes n'avaient pas les vraies couleurs de Zelda 3. Ils étaient...
tags:
  - solarus
title: Nouveaux graphismes du monde
---

Salut à tous !

J'ai refait les chipsets de la carte du monde, car les anciens graphismes n'avaient pas les vraies couleurs de Zelda 3. Ils étaient trop contrastés et trop sombres. Voici le résultat :

|    Avant    |    Après    |
| :---------: | :---------: |
| ![](63.png) | ![](62.png) |
