---
date: '2024-02-20'
excerpt: New pictures that unveil a day/night cycle for more immersion
tags:
  - games
  - mercuris
thumbnail: thumbnail.png
title: "Zelda : Mercuris' Chest do the show!"
---

Like last time, we unveil some pictures of our next Zelda fangame with 5 new screenshots.

## Day/night system

In january, we revealed the wide screen display, now it's focus on day/night system with those pictures. You can see that time is running in this world and that will have consequences on events that will happen.

![Sunset on the lake](mc_dusk.png)

![Night in a village](mc_night.png)

![Sun is rising at the bay](mc_dawn.png)

![An inaccessible chest](mc_dungeon_1.png)

![Gibdos are tough](mc_dungeon_2.png)

Nothing revolutionary of course, other games made with Solarus do have such system already. But this "real time" cycle was part of our plans for years. Solarus is now quite advanced that creating those features is pretty easy.

Stay alert during this year for more info and pictures of the game.
