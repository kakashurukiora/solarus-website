---
date: '2023-04-01'
excerpt: We were waiting since 2002... Today is the day!
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: "Demo release for The Legend of Zelda: Mercuris' Chest"
---

## Today is the day!

On this day of **April 1st, 2023**, after 21 years of unbearable wait, we are proud to present you the demo for the game _The Legend of Zelda: Mercuris’ Chest_, available on Windows, MacOS and Linux.

The game is, at the moment, only available in French, but English is coming soon.

The promise from 2002 has been kept. The vaporware isn’t any more.

{{< youtube "SYw8QJ7DW6E" >}}

## How to play

1. Download [Solarus Launcher](/en/download/#gamer-package).
2. Download [the game](/en/games/the-legend-of-zelda-mercuris-chest/) (as a `.solarus` file).
3. Open the `.solarus` file with Solarus Launcher.

You may share your experience on the [game's Discord channel](https://discord.gg/TPWDr3HG) with the community.

Have fun, and see you in **2033** for the full game!

{{< game-thumbnail "the-legend-of-zelda-mercuris-chest" >}}

## Source code and bug reports

The demo's source code is available [on Gitlab](https://gitlab.com/solarus-games/games/zelda-mercuris-chest-demo), although we decided to keep the full game's source code private until the release to avoid any spoilers.

You can report bugs in the game's [Gitlab issues page](https://gitlab.com/solarus-games/games/zelda-mercuris-chest-demo/-/issues) by creating issues. Please add any information that might help us: steps to reproduce the bug, screenshots or videos, etc.
