---
date: '2023-04-01'
excerpt: On attendait ça depuis 2002... C'est aujourd'hui le grand jour !
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: "Sortie de la démo pour The Legend of Zelda: Mercuris' Chest"
---

## C’est aujourd’hui !

En ce jour du **premier avril 2023**, après 21 ans d’attente insoutenable, nous sommes fiers de vous présenter la démo du jeu _The Legend of Zelda: Mercuris’ Chest_, disponible sur Windows, MacOS et Linux.

Le jeu est, pour le moment, disponible uniquement en français, mais l'aanglais ne devrait pas tarder.

La promesse de 2002 a été tenue. Le vaporware n’est plus.

{{< youtube "SYw8QJ7DW6E" >}}

## Comment jouer

1. Téléchargez [Solarus Launcher](/fr/download/#gamer-package).
2. Téléchargez [le jeu](/fr/games/the-legend-of-zelda-mercuris-chest/) (sous forme de fichier `.solarus`).
3. Ouvrez le fichier `.solarus` avec Solarus Launcher.

Venez partager votre expérience sur le jeu avec la communauté dans la [discussion Discord](https://discord.gg/TPWDr3HG).

Amusez-vous bien, et rendez-vous en **2033** pour le jeu complet !

{{< game-thumbnail "the-legend-of-zelda-mercuris-chest" >}}

## Code source et rapport de bugs

Le code source de la démo est disponible sur [sur Gitlab](https://gitlab.com/solarus-games/games/zelda-mercuris-chest-demo), cependant nous avons décidé de garder le code source du jeu complet privé jusqu'à la sortie pour éviter tout divulgâchage.

Vous pouvez rapporter les bugs sur [la page d'_issues_ Gitlab](https://gitlab.com/solarus-games/games/zelda-mercuris-chest-demo/-/issues) en créant un ticket. Ajoutez s'il vous plaît toute information susceptible de nous aider : étapes pour reproduire le bug, captures d'écran ou vidéos, etc.
