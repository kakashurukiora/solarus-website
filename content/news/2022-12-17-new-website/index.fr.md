---
date: '2022-12-17'
excerpt: 'Cadeau de Noël pour les fans de Solarus : un nouveau site web !'
tags:
  - solarus
  - website
thumbnail: thumbnail.png
title: Un nouveau site web pour Solarus !
---

## Un nouveau site web

Nous sommes heureux d'annoncer la sortie d'un nouveau site web pour Solarus ! C'est un cadeau de Noël pour les fans de Solarus, et nous espérons que vous l'apprécierez.

Le site web est maintenant basé sur [Hugo](https://gohugo.io/), un générateur de site statique. Cela signifie que le site web est maintenant beaucoup plus rapide à charger, et il est également plus facile à maintenir. Le site web est toujours disponible en anglais et en français.

![Site web Solarus](website.png)

## Un nouveau logo

Solarus a également un nouveau logo que vous pouvez voir sur la [page Presse](/about/press-kit/), ainsi que des couleurs de marque améliorées.

Ce logo devrait être plus élégant et plus professionnel. Nous espérons que vous l'apprécierez.

![Logo Solarus](new-logo.png)

## Un nouveau site web pour la documentation

La documentation est maintenant disponible sur un site web séparé : [docs.solarus-games.org](https://docs.solarus-games.org/). Ce site web est basé sur [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/), un thème pour MkDocs. Cela signifie que la documentation est maintenant beaucoup plus facile à lire et à naviguer.

L'API Lua n'est toujours pas disponible sur ce site web, mais nous travaillons dessus. En attendant, vous pouvez toujours accéder à la documentation de l'API Lua sur l'[ancien site web de documentation](https://doxygen.solarus-games.org/latest/).

![Documentation Solarus](documentation.png)

## Joyeux Noël !

Nous vous souhaitons un joyeux Noël et une bonne année ! Nous espérons que vous apprécierez le nouveau site web et le nouveau site web de documentation.
