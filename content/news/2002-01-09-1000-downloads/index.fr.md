---
date: '2002-01-09'
excerpt: Et oui ! Déjà 1 000 téléchargements de la démo de ZS depuis le début du mois de juillet 2001 ! Nous ne cachons pas que nous sommes heureux de...
tags:
- solarus
title: 1000 DOWNLOADS
---

![](1000.gif)Et oui ! Déjà 1 000 téléchargements de la démo de ZS depuis le début du mois de juillet 2001 ! Nous ne cachons pas que nous sommes heureux de ce résultat et on tient tous à vous remercier pour votre soutient ! ZS à l'air de plaire à une bonne partie des zeldamaniaks, on peut vous dire que le jour de la sortie de la version complète approche à grands pas... Le jeu complet n'aura plus rien à voir avec la démo, le jeu a été nettement amélioré et rendu plus passionant, plus fidèle au zelda original, enfin bref ! On vous promet que vous n'oublierez pas ce jeu comme pour ZELDA 3 : "Combien de temps mettrez-vous pour le terminer ?"
