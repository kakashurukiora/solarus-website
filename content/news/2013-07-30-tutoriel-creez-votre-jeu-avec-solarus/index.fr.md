---
date: '2013-07-30'
excerpt: Aujourd'hui je ne vais pas vous parler de nos jeux, mais de comment faire les vôtres ! J'ai le plaisir de vous annoncer que je démarre la toute...
tags:
- solarus
title: 'Tutoriel : Créez votre jeu avec Solarus !'
---

Aujourd'hui je ne vais pas vous parler de nos jeux, mais de comment faire les vôtres !

J'ai le plaisir de vous annoncer que je démarre la toute première série de tutoriels sur la création de jeux avec le moteur Solarus !

Ces tutoriels en vidéo auront pour but de vous montrer pas à pas les différents aspects du développement de votre propre jeu 2D en utilisant Solarus, de l'utilisation de l'éditeur de quêtes à la programmation en langage Lua. Solarus en étant seulement à la version 1.0, première version réellement utilisable par le public), certaines choses se font à la main (en modifiant des fichiers textes) et d'autres graphiquement dans l'éditeur de quête (comme dessiner les maps, placer les ennemis, etc.).

Vous pouvez soit commencer à partir d'une quête vide, ou modifier une quête existante comme Zelda Mystery of Solarus DX. Ce qui est sûr, c'est que la [documentation officielle](http://www.solarus-games.org/doc/latest/) deviendra vite une de vos pages favorites :)

Pour le moment, un premier épisode intitulé « Mise en route » est disponible. J'y explique comment créer un projet vide et afficher une petite image !

{{< youtube "9n-aottUQUA" >}}

À regarder de préférence en haute résolution (720p) sinon le texte ne sera pas lisible.

Vous êtes de plus en plus nombreux à nous demander de l'aide et des informations sur l'utilisation du moteur pour créer vos propres jeux. J'espère donc que cette série de tutoriels vous sera utile ! N'hésitez pas à commenter, et à me proposer les sujets que vous voudriez voir abordés dans les prochains épisodes. Il y a beaucoup de choses à voir mais je n'ai pas encore décidé d'un ordre précis :)
