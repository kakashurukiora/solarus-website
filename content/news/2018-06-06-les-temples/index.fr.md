---
date: '2018-06-06'
excerpt: Ces derniers temps, nous avons travaillé sur le mapping de la partie est de Cocolint. Je suis certain que vous n'aurez pas de mal à reconnaùtre le...
tags:
  - solarus
title: Les temples
---

Ces derniers temps, nous avons travaillé sur le mapping de la partie est de Cocolint. Je suis certain que vous n'aurez pas de mal à reconnaître le temple du nord et le temple du sud :)

![](1-300x240.png)

![](2-300x240.png)

Comme d'habitude, n'hésitez pas à donner votre avis sur notre travail. :)
