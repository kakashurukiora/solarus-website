---
date: '2023-04-02'
excerpt: A patch version that fixes a lot of bugs in the demo.
tags:
  - games
  - zelda
thumbnail: thumbnail.png
title: "Bugfix release 0.1.1 for The Legend of Zelda: Mercuris' Chest demo"
---

## A lot of fixes!

Following the first release of the _The Legend of Zelda: Mercuris' Chest_ demo yesterday, many of you have been playing and sharing your impressions with us. The team would like to thank you sincerely for the welcome you have given to the game.

You also reported a few bugs that remain into this first release and we quickly went into fixing them.

Today, we therefore offer you a new **0.1.1 version** which fixes most of the bugs that we spotted with your help yesterday.

Click below to go to the game page and download this patch version.

{{< game-thumbnail "the-legend-of-zelda-mercuris-chest" >}}

## Still only in French?

Yes, this version does not contain English on-screen texts yet. We will soon announce our plan to bring you an English version of the game in the future weeks.

## Reminder: How to play

1. Download [Solarus Launcher](/en/download/#gamer-package).
2. Download [the game](/en/games/the-legend-of-zelda-mercuris-chest/) (as a `.solarus` file).
3. Open the `.solarus` file with Solarus Launcher.

We are still looking forward for your reactions and thoughs on our [Solarus Discord server](https://discord.gg/TPWDr3HG).

## Keep up reporting us bugs

Go to the [Gitlab issues of the demo](https://gitlab.com/solarus-games/games/zelda-mercuris-chest-demo/-/issues) page (Gitlab account required) or directly on [Discord](https://discord.gg/TPWDr3HG).

As always, be specific enough to help us best to reproduce the bug. If you want to report on Discord, remember to put your screenshots or explanations in spoiler if they risk revealing things to other players.
