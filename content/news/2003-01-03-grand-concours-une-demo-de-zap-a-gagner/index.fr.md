---
date: '2003-01-03'
excerpt: Le samedi 25 janvier à 21 h (15 h heure québécoise), nous organiserons un concours sur le chat de Zelda Solarus. Ce concours consistera en un...
tags:
- solarus
title: 'Grand concours : une démo de ZAP à gagner !'
---

Le **samedi 25 janvier à 21 h** (15 h heure québécoise), nous organiserons un concours sur le chat de Zelda Solarus. Ce concours consistera en un quizz sur notre création : Zelda Mystery of Solarus. Les questions seront diverses : elles porteront sur les objets, sur les personnages, sur les énigmes, etc...

Nous fixons cette date longtemps à l'avance pour qu'un maximum de personnes soient présentes. Le gagnant du quizz aura tout simplement droit à la démo de Zelda : Advanced Project en avant-première, en même temps que les testeurs officiels !

Si vous êtes pressés de jouer à notre prochaine création, ne manquez donc pas cette occasion unique ! Parlez-en autour de vous :-)

Nous vous préciserons les modalités et les règles du concours quelques jours avant le 25 janvier. En attendant, je vous conseille de rejouer à ZS afin de répondre le mieux possible aux questions du quizz !
