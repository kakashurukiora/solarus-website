---
date: '2016-07-27'
excerpt: On July 27th of 2006, I wrote the first commit of the Solarus project.10 years later, the project has grown a lot! Today, we are proud to announce...
tags:
  - solarus
title: Solarus 1.5 released, now with a quest launcher!
---

On July 27th of 2006, I wrote the [first commit](https://github.com/christopho/solarus/commit/2927325) of the Solarus project.10 years later, the project has grown a lot! Today, we are proud to announce that Solarus and Solarus Quest Editor were just released in version 1.5.The development of 1.5 took more than one year and this is the best release ever.

There is now a nice quest launcher window:

![solarus-launcher](solarus-launcher.png)

The quest launcher allows you to add or remove quests and to play them. A console shows the output of the quest. You can also edit audio and video settings from this window. Of course, the old way of running quests still exists if you don't want this window. But this is more user friendly and we already have ideas of new features for the launcher for future releases :)

Aside from the quest launcher, we added a lot of new features to the quest editor and to the Lua API.

- Maps can have more than 3 layers.
- Smart resizing! Full rooms can now be resized in one operation.
- Directional resizing! Entities can now be resized to the left and top, not only to the right and bottom.
- Musics and sounds can now be played from the editor!
- Support for custom loops in OGG musics!
- A console in the quest editor to execute Lua code on the fly!
- An initial quest with many sprites, musics and sounds thanks to Diarandor and Eduardo!
- Improved performance when editing or running maps with lots of entities.
- The camera is now a map entity, you can customize its size and movement easily.

And a lot more, see the full changelog below.Your quest scripts probably need some adjustements to upgrade to 1.5. All of this is explained in the [migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide).

- [Download Solarus 1.5](http://www.solarus-games.org/download/ 'Download Solarus')
- [Solarus Quest Editor](http://www.solarus-games.org/engine/solarus-quest-editor/ 'Solarus Quest Editor')
- [Lua API documentation](http://www.solarus-games.org/doc/latest/lua_api.html 'LUA API documentation')
- [Migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide)

We are very proud of this new release. Actually, some quest makers already switched to 1.5 a while ago because they could not wait to test the new features! So the release is for the most part validated. If you find bugs or if you have suggestions of features, don't hesitate to report them on the [GitHub page of our projects](https://github.com/christopho/) if they are not already present. If this release is the best release ever, this is largely thanks to your feedback guys.

I am now working on a brand new video tutorial series to help you create your quest. Because the old ones are starting to be really obsolete! To make sure you don't miss their publication, you should subscribe to my [YouTube channel ChristophoZS](https://www.youtube.com/channel/UCB-tYDasLr9riVLh3DgiGgw) and my [Twitter @ChristophoZS](https://twitter.com/ChristophoZS).As always, our games ZSDX, ZSXD and Zelda ROTH SE were also upgraded to give you up-to-date quest examples.

- [Download Zelda Mystery of Solarus DX 1.11](http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/ 'Download ZSDX')
- [Download Zelda Mystery of Solarus XD 1.11](http://www.solarus-games.org/games/zelda-mystery-of-solarus-xd/ 'Download ZSXD')
- [Download Zelda Return of the Hylian SE 1.1](http://www.solarus-games.org/games/zelda-return-of-the-hylian-se 'Download ZSXD') (now available in Spanish and German! thanks Diarandor and Wuzzy2!)

We are doing all of this on our free time and this is all free and open-source software. If you like our work, please consider [making a donation](http://www.solarus-games.org/community/donate/). It will contribute for the server fees and to the motivation of the team!

Here is the full changelog. Sit well!

## Changes in Solarus 1.5

New features:

- Add a launcher GUI to ease chosing a quest and setting options (#693).
- Rename the solarus_run executable to solarus-run.
- Add version number and symbolic links when building the library.
- Add a `-lua-console` option to run Lua code from the standard input.
- Remove the `-win-console` option, the preferred way is now to use a GUI.
- Add a `-turbo` option to run at full speed.
- Add a `-lag` option to simulate slower systems for debugging.
- Print when the main loop starts and stops.
- Print the Lua version at startup (#692).
- Outputs are now prefixed by `[Solarus]` and the current simulated time.
- Musics: Add support of custom OGG looping (#643).
- Maps: allow more than 3 layers (#445).
- Improve the performance of loading big maps (#854).
- Improve the performance of custom entity collisions.
- Improve the performance of collisions by using a quadtree.
- Entities far from the camera are no longer suspended.
- The hero no longer automatically jumps when arriving on water (#530).
- Destinations can now set to update or not the starting location (#819).
- Teletransporters on the side of the map now work on all layers (#850).
- Streams can now have a speed of zero (#496).

Lua API changes that introduce incompatibilities (see the [migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide) for instructions):

- Fix missing collision detections and entity notifications.
- chest:on_empty() is replaced by chest:on_opened(treasure) (#483).
- Enemy ranks no longer exist, set_hurt_style() needs to be called (#449).
- Items with amount now have a default max amount of 1000 (#688).
- New ability "jump_over_water" in game:get/set_ability(), off by default (#530).
- Fix hero state name "freezed", renamed it to "frozen" (#813).
- Fix map:get_entities() not returning the hero (#670).
- Fix map:create_custom_entity() not erroring when width/height are missing.
- map:get_camera_position() is now deprecated, use camera:get_bounding_box().
- map:move_camera() is now deprecated, use a camera movement instead.
- map:draw_sprite() is now deprecated, use map:draw_visual() instead (#661).
- Fix entity:set_enabled(true) delayed while it blocks the hero (#817).
- Fix brandished treasure sprite and shop treasure sprite not animated (#790).
- circle_movement:get/set_initial_angle() now use degrees (#721).
- Add ability to hide mouse cursor (#891).

Lua API changes that do not introduce incompatibilities:

- Add a function sol.main.get_solarus_version() (#767).
- Add a function sol.main.get_quest_format().
- Add a function sol.main.get_type() (#744).
- Add a method game:set_suspended() (#845).
- Add methods map:get_min_layer() and map:get_max_layer() (#445).
- Add a method map:get_entities_by_type() (#796).
- Add a method map:get_entities_in_rectangle() (#142).
- Add a method map:draw_visual() to draw any drawable object (#661).
- Add a method map:get_camera() (the camera is now a map entity).
- Add methods map:set_world() and map:set_floor() (#656).
- map:get_entities() can now be called without parameter to get all entities.
- map:get_entities\*() functions now give entities sorted in Z order (#779).
- Add an event entity:on_movement_started().
- Add a method entity:get_max_bounding_box() considering sprite boxes (#754).
- entity:get_center_position() now also returns the layer.
- Add a method entity:get_facing_position().
- Add a method entity:get_facing_entity() (#877).
- Add a method entity:get_ground_position() (#830).
- Add a method entity:get_ground_below() (#830).
- entity:set_optimization_distance() is now only a hint for the engine.
- entity:test_obstacles() now also works without parameters.
- entity:overlaps() now has an optional collision mode parameter (#748).
- Add entity:get_sprite() to all entities, with an optional name value (#669).
- Add a method entity:get_sprites() (#851).
- Add methods entity:bring_sprite_to_front/back() (#809).
- enemy/custom_entity:create_sprite() now take an optional name value.
- hero:save_solid_ground() can now take a function parameter (#667).
- Add a method hero:start_attack() (#821).
- Add methods npc:is/set_traversable() (#712).
- Add methods chest:get/set_treasure() (#664).
- Add an event chest:on_opened() with treasure info parameters (#483).
- Add methods dynamic_tile:get_pattern_id() and get_modified_ground() (#755).
- Add methods destination:get/set_starting_location_mode() (#819).
- Add a method switch:is_walkable() (#729).
- Add a method switch:is_locked().
- Add a method sprite:get_num_frames() (#818).
- Add methods sprite:get_size() and sprite:get_origin() (#823).
- sprite:set_animation() now takes an optional callback parameter (#861).
- Add a method surface:get_opacity() (#722).
- Add methods surface/text_surface/sprite:get/set_blending_mode (#930).

Bug fixes:

- Fix crash when main.lua has a syntax error.
- Fix crash with missing directions in sprites controlled by the engine (#864).
- Fix sprite:on_animation_finished() and others not working sometimes (#799).
- Fix error in sprite:set_animation() when the direction is missing (#937).
- Fix straight movement precision.
- Fix freeze when loading a map with tiles outside the limits (#875).
- Fix crash when trying to use a non-saved item (#889).
- Fix sword tapping sound still played when the game is suspended (#797).
- Fix hero:set_invincible() not working without duration (#805).
- Fix lifted item walking animation only accepting 3 frames (#645).
- Fix enemy:set_attack_consequence_sprite() with thrown items (#834).
- Fix custom_entity:set_can_traverse() for doors (#716).
- Fix custom_entity:set_can_traverse_ground() for some grounds (#794).
- Fix custom entity collisions missed for entities that do not move (#671, #883).
- Fix custom_entity:get_modified_ground() returning nothing.
- Fix custom_entity:on_ground_below_changed() not called (#738).
- Fix missing notifications in custom_entity:set_origin() (#880).
- Fix creating an entity with the same name as another one just removed (#795).
- Fix parallax scrolling for dynamic tiles (#816).
- Fix crash when a diagonal tile is not square (#837).
- Fix crash when the teletransporter after stairs is missing.
- Fix non-blocking stream turns after going south (#648).
- Fix text_surface:set_rendering_mode() not working (#833).
- Fix possible freeze when changing the position of a path finding entity.
- Fix circle_movement:set_initial_angle() not working (#721).
- Fix straight movement setting speed to zero when reaching obstacles (#633).
- Fix support of joypads with multiple axes.
- Fix sol.input.get_mouse_coordinates() ignoring the zoom factor (#734).

## Changes in Solarus Quest Editor 1.5

New features:

- Add an execution log view of the quest.
- Add a console to execute Lua instructions when the quest is running.
- Add a command-line option -run to run a quest instead of opening the GUI.
- Allow to save all open files at once (#98).
- Allow to close all open files at once.
- Allow to close the current quest (#88).
- Allow to reorder tabs (#107).
- Allow to switch tabs with keyboard shortcuts (#101).
- Allow to automatically save files before running the quest (#90).
- Allow to restore open tabs at startup (#131).
- Add a recent quests menu (#89).
- Creating a new quest now provides a lot of initial resources (#201).
- Creating a new quest now sets the write directoy to a unique id (#207).
- Resource directories are now automatically created when opening a quest.
- The shortcut to load a quest is now Ctrl+L.
- Add a menu item to open quest properties (#206).
- Allow to update teletransporters when changing the id of a map (#56).
- Allow to update teletransporters when renaming a destination (#58).
- Allow to update maps when changing the id of a tileset (#57).
- Add resource dialog: set the id as initial value for the description (#192).
- Initialize map/item/enemy/entity scripts with helpful code (#33).
- Automatically add .lua extension when creating a script file (#46).
- Allow to play musics and sounds from the quest tree (#8).
- Allow to play music and sounds from music and sound selectors.
- Show the sprite of each element in enemy and item selectors (#9).
- Map editor: allow more than 3 layers.
- Map editor: implement smart resizing to resize full rooms at once (#54).
- Map editor: allow to resize from the left and from the top (#111).
- Map editor: shortcuts for showing/hiding layers are now 0, 1 and 2, etc.
- Map editor: add shortcuts to show/hide entity types (#82).
- Map editor: allow to show/hide obstacles (#6).
- Map editor: add shortcuts to move one layer up or down (+ and -).
- Map editor: add Escape shortcut to stop adding entities (#92).
- Map editor: new entity icons by Amine (#119).
- Map editor: show the real variant of pickables (#109).
- Tileset editor: propose to refresh the image when it has changed (#86).
- Tileset editor: don't force the user to close maps before renaming patterns.
- Tileset editor: add settings (#178).
- Sprite editor: check the value of frame to loop on (#203).
- Sprite editor: auto-detect the grid size (#13).
- Settings: add sprite editor options.
- Add select all to map, tileset and text editors (#106).
- Add unselect all to map, tileset and text editors (#115).

Bug fixes:

- Fix wrong hero movements sometimes when playing from the editor (#138).
- Fix tabs not closed when opening another quest (#44).
- Fix the drop-down list of resource selectors too small on some systems.
- Map editor: fix crash when creating a block with a missing sprite (#195).
- Map editor: fix performance regression when pasting a lot of entities (#141).
- Map editor: fix entities still shown when put on a hidden layer.
- Map editor: fix precision when starting selection above existing entities.
- Map editor: stop adding tiles when unselecting them from the tileset view.
- Map editor: uncheck entity creation button after adding an entity (#105).
- Map editor: keep tileset scroll position when refreshing/changing it (#129).
- Map editor: improve performance of selecting multiple entities.
- Map editor: improve performance of moving multiple entities.
- Map editor: fix multiple default destinations when copy-pasting (#118).
- Tileset editor: improve performance of deleting multiple tile pattenrs (#120).
- Text editor: improve tabulation behavior (#43).
- Text editor: fix multiline comments matching to the end of the line (#63).
- Sprite editor: fix "missing image" message staying after solving it (#191).
- Quest properties: don't allow slashes or backslashes in the write dir (#59).

## Changes in Zelda Mystery of Solarus DX 1.11

Bug fixes:

- Fix wrong layer when leaving dungeon 10.
- Dungeon 2: fix sound played twice when activating the statues.
- Dungeon 9: fix geting back to room entrance when touching spikes.
- English dialogs: rename Pegasus Shoes to Pegasus Boots.

## Changes in Zelda Mystery of Solarus XD 1.11

Bug fixes:

- Fix typo in French dialogs.

## Changes in Zelda Return of the Hylian SE 1.1

New features:

- Add German translation (thanks Wuzzy2!).
- Add Spanish translation (thanks Diarandor!).

Bug fixes:

- Fix initial cursor position in savegame menu after a new game.
- Fix stuck in a cliff west of the castle (#87).
- Fix silver arrows incorrectly changing the arrow counter (#88).
- Fix stats showing 47/46 monsters if a chicken was killed (#89).
- Dungeon 4: fix crash when picking the crystal as rabbit (#91).
