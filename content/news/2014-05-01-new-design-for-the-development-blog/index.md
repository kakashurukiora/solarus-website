---
date: '2014-05-01'
excerpt: As the project grows and gets bigger, we thought it was important to get a new design for the development blog. Here it is ! We merged some pages to...
tags:
- solarus
title: New design for the development blog
---

As the project grows and gets bigger, we thought it was important to get a new design for the development blog. Here it is ! We merged some pages to simplify navigation and changed the wordpress theme to a customized version of the very nice Vantage theme by SiteOrigin.

Solarus is about to be released in its 1.2 version, so let's say the new design is to celebrate the soon-to-come event !
