---
date: '2011-08-12'
excerpt: Our parodic creation Zelda Mystery of Solarus XD is now available in English, as announced in my previous post. You can find it on the download page...
tags:
- solarus
title: Download ZSXD now!
---

Our parodic creation Zelda Mystery of Solarus XD is now available in English, as announced in my previous post. You can find it on the [download page of ZSXD](http://www.zelda-solarus.com/jeu-zsxd-download&lang=en) on Zelda Solarus (the website is in French but the download page exists in English). You can also download it directly with one of the links below:

- Windows: [installer](http://www.zelda-solarus.com/download-zsxd_win32_setup) or [zip file](http://www.zelda-solarus.com/download-zsxd_win32_zip)
- Debian or Ubuntu : [32 bit](http://www.zelda-solarus.com/download-zsxd_debian-i386) or [64 bit](http://www.zelda-solarus.com/download-zsxd_debian-amd64) (to play, the command to run is "Solarus")
- [Source code](http://www.zelda-solarus.com/download-zsxd_src)

Have fun! But don't forget: this game is essentially a big April 1st joke ;)
