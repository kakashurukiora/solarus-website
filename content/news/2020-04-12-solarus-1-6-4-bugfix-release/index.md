---
date: '2020-04-12'
excerpt: A series of important bugfixes, along with a brand new free tileset.
tags:
- solarus
thumbnail: cover.jpg
title: Bugfix release 1.6.4 for Solarus
---

We have just published a bugfix release for Solarus, which is now at version **1.6.4**. The short-lived **1.6.3** version has been released yesterday, but has been immediately followed by the next one, because of an important bugfix.

## Changelog

Here is the exhaustive list of all changes brought by Solarus 1.6.4. Thanks to all contributors.

### Changes in Solarus 1.6.4

![Solarus logo](engine_logo.png)

#### Engine changes

- Fix macOS port not working since 1.6.1 due to OpenGL.
- Fix shaders compilation in some OpenGL ES 3.0 platforms (#1437).
- Fix UTF-8 quest path encoding handling (#1429).
- Fix UTF-8 file names in Lua Open File API for Windows (#1413).
- Fix memory usage when visiting very large maps (#1395).
- Fix crash when removing the shadow of a carried object (#1423).
- Fix crash when stopping the movement of a thrown object (#1452).
- Fix carried object shadow still displayed after removing it (#1436).
- Fix animating hero sprites dynamically created (#1348).
- Fix hero speed when going from deep water to shallow water (#1186).
- Fix trail sprite not taking the hero's direction (#1464).
- Fix ground sprite still displayed while jumping (#1458).
- Fix custom states wrongly affected by ground speeds (#1416).
- Fix custom states not activating side teletransporters (#1448).
- Fix streams continuing to act when the layer has changed.
- Fix enemy:on_dying() not called when setting enemies life to zero (#1440).
- Fix talking to NPCs while swimming (#1043).
- Fix creating dynamic sprite with tileset anims (#1461).

#### Lua API changes

This release does not add new features to the Lua API.

### Changes in Solarus Launcher 1.6.4

![Solarus Launcher logo](launcher_logo.png)

- Fix UTF-8 quest path encoding handling (#1429).
- Add drag and drop functionality to add new quests (#1420).

### Changes in Solarus Quest Editor 1.6.4

![Solarus Quest Editor logo](editor_logo.png)

- Add Ocean Set tilesets to the initial quest (#478).
- Map editor: fix contours generated tileset specific (#485).
- Map editor: fix a crash when undoing changing the layer of entities (#471).
- Tileset editor: save the update maps option when renaming patterns (#483).
- Tileset editor: fix moving patterns to an overlapping position (#477).
- Tileset editor: fix crash when a tileset file changes while dragging (#476).
- Fix possible crash when closing the window.
- Fix opening local documentation in Windows.
- Fix tiles getting unwanted enabled_at_start field in map data files.
- Fix spin boxes breaking the UI on Windows (#486).

### Changes in Solarus Free Resource Pack 1.6.4

![Solarus Free Resource Pack logo](free_resource_pack_logo.png)

- Update Ocean's Hearts tilesets.
- Update OceanSet tilesets.
- Fix sword running tunic animation.

### Changes in Solarus ALTTP Pack 1.6.2

![Solarus ALTTP Pack logo](alttp_logo.png)

- Add pike enemies.
- Add big chest sprite.
- Fix HUD disappearing after game-over (#38).
- Fix bari, eyegore and lynel enemies.
