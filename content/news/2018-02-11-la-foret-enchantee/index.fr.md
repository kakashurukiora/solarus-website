---
date: '2018-02-11'
excerpt: Cette semaine, nous avons bien travaillé sur les personnages du jeu A Link to the Dream et sur la forêt enchantée. Cette dernière est vraiment...
tags:
  - solarus
title: La forêt enchantée
---

Cette semaine, nous avons bien travaillé sur les personnages du jeu A Link to the Dream et sur la **forêt enchantée**. Cette dernière est vraiment aboutie à présent !

Voici quelques images de ce lieu très important dans le début du jeu.

![](4-1-300x240.png

![](3-1-300x240.png

![](2-1-300x240.png

![](1-1-300x240.png)

Je suis certain que vous n'aurez aucun mal à reconnaitre les différents endroits qui sont représentés sur ces images.
