---
date: '2002-11-25'
excerpt: Les bonnes choses ont malheureusement une fin ! Nous avons stoppé cette opération qui faisait apparemment plaisir à pas mal de monde mais pour...
tags:
- solarus
title: 'Opération 1 Screen par jour : c''est fini'
---

Les bonnes choses ont malheureusement une fin ! Nous avons stoppé cette opération qui faisait apparemment plaisir à pas mal de monde mais pour éviter de trop vous en montrer, vous devrez vous contenter de ces écrans jusqu'à la version démo tant convoitée prévue pour Février...
