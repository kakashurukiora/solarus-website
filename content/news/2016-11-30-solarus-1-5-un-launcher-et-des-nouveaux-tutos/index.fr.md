---
date: '2016-11-30'
excerpt: Ça bouge beaucoup du côté du moteur Solarus et de Solarus Quest Editor ! Les tutos vidéo de niveau basique sont terminés et publiés, et Solarus...
tags:
- solarus
title: Solarus 1.5, un launcher et des nouveaux tutos !
---

Ça bouge beaucoup du côté du moteur Solarus et de Solarus Quest Editor ! Les tutos vidéo de niveau basique sont terminés et publiés, et Solarus 1.5.1 vient de sortir. Rappelons d'abord que la version 1.5, disponible depuis cet été, vous propose comme grande nouveauté un lanceur de quête :

![](solarus-launcher.png)

Ce lanceur (ou launcher) vous permet de choisir la quête que vous voulez exécuter, et de configurer plusieurs options. Mais c'est loin d'être la seule nouveauté :

- Les maps peuvent maintenant avoir plus de 3 couches.
- Redimensionnement intelligent ! Vous pouvez maintenant redimensionner une salle entière d'un seul coup.
- Redimensionnement directionnel ! Les entités peuvent maintenant être étirées vers la gauche ou le haut, plus seulement vers la droite et le bas.
- Les musiques et les sons peuvent maintenant être joués depuis l'éditeur de quêtes !
- Le moteur supporte maintenant les boucles personnalisées dans les musiques OGG.
- Une console vous permet d'exécuter du code Lua à la volée dans l'éditeur de quêtes.
- La quête initiale contient maintenant de nombreux sprites, musiques et sons grâce à Diarandor et Eduardo.
- Performances améliorées lors de l'édition ou de l'exécution de maps avec beaucoup d'entités.
- La caméra est maintenant une entité de la map, vous pouvez personnaliser sa taille et son mouvement facilement.

Liste complète des changements : ici pour la [version 1.5.0](http://www.solarus-games.org/2016/07/27/solarus-1-5-released-now-with-a-quest-launcher/) et [ici pour la version 1.5.1](http://www.solarus-games.org/2016/11/29/bugfix-release-1-5-1-and-improved-sample-quest/) toute fraîche

Télécharger [Solarus 1.5.1 + l'éditeur de quêtes](http://www.solarus-games.org/downloads/solarus/win32/solarus-1.5.1-win32.zip) pour Windows

- Télécharger le [code source](http://www.solarus-games.org/downloads/solarus/solarus-1.5.1-src.tar.gz)
- [Blog de développement Solarus](http://www.solarus-games.org/)
- Comment [convertir votre quête](http://wiki.solarus-games.org/doku.php?id=migration_guide#solarus_14_to_solarus_15) de Solarus 1.4.x vers Solarus 1.5.x

### Tutoriels vidéo et live-codings

Solarus Quest Editor a tellement changé depuis mes premiers tutoriels que je les ai recommencés entièrement ! Pour le moment, 33 épisodes de niveau basique sont disponibles en français et en anglais, et j'en publie un nouveau chaque samedi. Les prochains vont être de niveau intermédiaire et avancé, je vous réserve des exemples de plus en plus évolués pour la suite ! J'essaie de rendre les tutos les plus indépendants possibles les uns des autres pour que vous puissiez directement aller à ceux qui vous intéressent.

- [Tutoriels Solarus en vidéo](https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufzB4nXkSINFhbrtLlnlI4li)
- [Chaîne YouTube ChristophoZS](https://www.youtube.com/channel/UCB-tYDasLr9riVLh3DgiGgw)
- [Twitter @ChristophoZS](https://twitter.com/ChristophoZS) : abonnez-vous pour ne rien rater !

Je fais aussi de temps en temps des sessions de live-coding ou de live-making, c'est-à-dire des streamings vidéo de mon écran pendant que je développe Solarus, Solarus Quest Editor mais surtout aussi mes nouveaux projets de jeux : Zelda Mercuris' Chest, Zelda Oni-Link Begins Solarus Edition et plus. Il n'y a pas de planning régulier pour les streamings (contrairement aux tutos du samedi) mais les streamings sont toujours annoncés à l'avance sur mon fil [Twitter @ChristophoZS](https://twitter.com/ChristophoZS).

Ce qui est sûr c'est que les tutos aussi bien que les live-codings ont beaucoup de succès et que leur activité ne va pas se calmer !
