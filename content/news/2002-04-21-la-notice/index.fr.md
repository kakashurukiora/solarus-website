---
date: '2002-04-21'
excerpt: A quatre jours de la sortie officielle de ZS, je vous propose dès maintenant la notice du jeu sur le site, histoire de vous faire (im)patienter un...
tags:
- solarus
title: La notice !
---

A quatre jours de la sortie officielle de ZS, je vous propose dès maintenant la notice du jeu sur le site, histoire de vous faire (im)patienter un peu plus. Si vous téléchargez le jeu, la notice sera également installée (au format HLP). Cette notice, comme toute notice qui se respecte, rappelle l'histoire du jeu, explique les commandes, présente notamment les persos, le menu de pause et les objets. Une page "conseils" est également là pour vous donner quelques astuces utiles.

[Lire la notice](http://www.zelda-solarus.com/jeu-zs-notice)
