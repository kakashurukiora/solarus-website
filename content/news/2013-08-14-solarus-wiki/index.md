---
date: '2013-08-14'
excerpt: Christopho began to make video tutorials about how to make a game with Solarus. However, these tutorials are, for the moment, only in French. To help...
tags:
- solarus
title: Solarus Wiki
---

Christopho began to make video tutorials about how to make a game with Solarus. However, these tutorials are, for the moment, only in French. To help the community to get the basis of Solarus, a [Wiki](http://wiki.solarus-games.org/) where everyone can contribute has been set up. We use [Dokuwiki](https://www.dokuwiki.org/), a free and open-source file-based wiki engine.

- [Solarus wiki](http://wiki.solarus-games.org/)

Currently there is almost nothing on the Wiki, but we are working on it, and we hope you too !
