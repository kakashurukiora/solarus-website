---
date: '2002-03-09'
excerpt: On ne vous a toujours pas montré à quoi ressemblait la carte du monde de ZELDA SOLARUS. La carte est définitive, elle ne changera pas, 8 niveaux...
tags:
- solarus
title: Carte extérieure
---

On ne vous a toujours pas montré à quoi ressemblait la carte du monde de ZELDA SOLARUS.

![](Carte-exterieur.png)

La carte est définitive, elle ne changera pas, 8 niveaux dans le jeu sont présents sur cette carte. Vous aperçevez le village au centre de la carte, la montagne des terreurs au nord, le château d'hyrule juste endessous. On aperçoit également des lieux portant de nom de Beaumont, Bois perdus... Vous pourrez consulter cette carte pendant votre aventure et vous situer facilement dans ce monde.
