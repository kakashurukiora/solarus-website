---
date: '2001-07-12'
excerpt: Salut à tous ! Cette mise à jour est consacrée à l'intro du jeu. L'intro présente l'histoire, et en particulier les événements qui se sont...
tags:
  - solarus
title: Tous les screenshots de l'intro
---

Salut à tous !

Cette mise à jour est consacrée à l'intro du jeu. L'intro présente l'histoire, et en particulier les événements qui se sont déroulés entre le scénario de A Link to the Past et celui de Mystery of Solarus. Vous pouvez d'ailleurs reconnaître certains écrans tirés de la séquence de fin de Zelda 3 :

[](solarus-ecran28.png)

[](solarus-ecran29.png)

[](solarus-ecran30.png)

[](solarus-ecran31.png)

[](solarus-ecran32.png)

Pour connaître le scénario complet du jeu, rendez-vous sur la page [correspondante](http://www.zelda-solarus.com/scenario.php3). À bientôt !
