---
date: '2013-08-07'
excerpt: "Two new types of grounds (or terrains) are now implemented in Solarus 1.1, as well as improvements in the detection of the ground. \tIce ground:..."
tags:
- solarus
title: Ice, low walls and other improvements about grounds
---

Two new types of grounds (or terrains) are now implemented in Solarus 1.1, as well as improvements in the detection of the ground.

- Ice ground: tiles that make the hero slide, with some sort of inertia.
- Low walls: obstacles that can only be traversed by projectiles like thrown items, the boomerang, arrows and flying enemies.

I also improved the code that detects the kind of ground of a point. Before, it only worked with the hero, and did not always correctly take into account dynamic entities that may change the ground, like dynamic tiles. (And the code was too complex!)

Now, the ground is correctly detected, including when it gets changed by dynamic tiles (even with moving dynamic tiles). Last but not least, this new detection algorithm works for any coordinates (not only the hero). Therefore, enemies and thrown items can now fall into holes and drown into water or lava!

These improvements and the two new kinds of tiles were demanded for a long time. I hope you will enjoy them!
