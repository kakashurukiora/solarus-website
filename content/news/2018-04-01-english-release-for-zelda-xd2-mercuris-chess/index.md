---
date: '2018-04-01'
excerpt: Who said it was too late? It's never too late! Exactly one year after the original...
tags:
  - solarus
title: 'English release for Zelda XD2: Mercuris Chess'
---

**Who said it was too late? It's never too late!**

{{< youtube "M2tAk5hRMHk" >}}

Exactly **one year after the original release** of the sequel to [our first parodic game](http://www.solarus-games.org/games/zelda-mystery-of-solarus-xd/), we are proud to finally offer an to the English speakers the opportunity to play our game in an **all-English version**. The translation wasn't easy : the original French release contains multiple references to French-speaking culture (Comic groups Les Nuls, Les Inconnus, cult movies like _Cité de la peur_ and _OSS 117 à Rio_, and of course the beloved cult TV show _Kaamelott_).

As a **direct sequel** (the story begins only months after the first episode), the overworld is expanded, the story is extended and the funny tone is kept, if not bettered!

![](box_zelda_xd2-300x214.png)[Click here to download the game](http://www.solarus-games.org/games/zelda-xd2-mercuris-chess/)

The game is longer than the first episode: it has **2 dungeons**, and **lots of side-quests** and secrets everywhere, and also features silly NPCs all around the overworld. Be sure to speak to every one to not miss a joke! The estimated game duration is **7-12h of playing**.

We hope that you will enjoy our little game. Have fun!
