---
date: '2024-01-14'
excerpt: Nous dévoilons de nouvelles images de notre prochaine création avec un format écran large digne de ce nom !
tags:
  - games
  - mercuris
thumbnail: thumbnail.png
title: "De nouvelles images de Zelda : Mercuris' Chest"
---

Cela faisait très longtemps que nous n'avions pas donné de nouvelles de notre fangame The Legend of Zelda : Mercuris' Chest.

Après la sortie de la démo le 1er avril 2023, nous avons bien entendu continué à travailler sur le jeu complet ; et tout au long de cette année, nous communiquerons sur les nouveautés très nombreuses que nous ajoutons au fur et à mesure.

## Grand écran

Première grosse annonce, comme vous le voyez sur les captures d'écran du jeu complet en développement, nous avons décidé de vous proposer une expérience en écran large !

![Un village bien gardé](screen_1.png)

![Discussion à Chrono Ville](screen_2.png)

![Chez les Gorons](screen_3.png)

A l'instar d'autres jeux comme Ocean's Heart, fini l'affichage 4/3 ! Mercuris' Chest est un fangame où vous assisterez à du grand spectacle sur tout l'affichage de votre écran.

## Date de sortie

Concernant la date de sortie du projet, nous n'avons pas encore d'idée précise à ce sujet. Restez à l'écoute des actualités car nous devrions en parler très prochainement.

En attendant, nous travaillons activement à faire de ce jeu un expérience vraiment différente à nos précédentes créations.
