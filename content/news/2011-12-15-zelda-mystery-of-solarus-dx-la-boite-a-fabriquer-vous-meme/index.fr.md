---
date: '2011-12-15'
excerpt: Pour pouvoir déposer Zelda Mystery of Solarus DX au pied du sapin, rien de telle qu'une boîte officielle !
tags:
  - solarus
title: 'Zelda Mystery of Solarus DX : la boîte à fabriquer vous-même !'
---

Bonsoir à tous,

Pour pouvoir déposer Zelda Mystery of Solarus DX au pied du sapin, rien de telle qu'une boîte officielle !

![](http://www.zelda-solarus.com/images/zsdx/boite_avant.jpg)

![](http://www.zelda-solarus.com/images/zsdx/boite_arriiere.jpg)

La boîte a été conçue et réalisée par Neovyse rien que pour vous !
Vous pouvez la télécharger, la découper et la monter vous-même en cliquant ci-dessous :

[![](http://www.zelda-solarus.com/images/zsdx/boite_a_monter.jpg)](http://www.zelda-solarus.com/zsdx/boite.pdf)

Un grand bravo à Neovyse pour ce travail, ses artworks et autres œuvres Zelda-esques :)

Enjoy ^\_^
