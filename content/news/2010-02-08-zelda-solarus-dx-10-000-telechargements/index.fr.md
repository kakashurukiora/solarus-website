---
date: '2010-02-08'
excerpt: La démo de Zelda Mystery of Solarus DX, disponible depuis le 18 décembre, vient de franchir la barre des 10 000 téléchargements. Les...
tags:
- solarus
title: 'Zelda Solarus DX : 10 000 téléchargements !'
---

La démo de Zelda Mystery of Solarus DX, disponible depuis le 18 décembre, vient de franchir la barre des 10 000 téléchargements.
Les statistiques montrent que c'est la version anglaise qui a été le plus téléchargée : il semble en effet que la sortie ait fait plus de bruit de manière internationale que sur les sites francophones. Quoi qu'il en soit, c'est une réussite :)

Pendant ce temps, la version complète avance bien. J'avance à la fois dans l'aventure (le deuxième donjon est en cours de réalisation) mais aussi pour améliorer certaines parties du moteur. Par exemple, à l'avenir il n'y aura plus qu'une seule version à télécharger pour toutes les langues, ce qui simplifiera la vie des développeurs, des traducteurs, des packageurs et des joueurs :P

Je prévois également d'ouvrir un blog dédié au développement du moteur de jeu. Ce blog sera destiné aux développeurs, aux personnes intéressées par le code source pour créer éventuellement d'autres projets, et aux personnes souhaitant contribuer, par exemple pour effectuer une traduction. Il sera en anglais étant donné que la portée du projet se veut internationale et qu'il s'adresse à des développeurs. Ce blog de développement donnera des informations sur le fonctionnement du moteur de jeu et pourra centraliser plusieurs projets réalisés avec le moteur (des Zelda ou non), même si pour l'instant, Zelda Mystery of Solarus DX est le seul qui existe.
