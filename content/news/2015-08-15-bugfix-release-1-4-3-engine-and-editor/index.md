---
date: '2015-08-15'
excerpt: Our new game Zelda Return of the Hylian (Solarus Edition) comes with a new bugfix release of Solarus and Solarus Quest Editor. A lot of issues that...
tags:
- solarus
title: Bugfix release 1.4.3 (engine and editor)
---

Our new game [Zelda Return of the Hylian](http://www.solarus-games.org/games/zelda-return-of-the-hylian-se/) (Solarus Edition) comes with a new bugfix release of Solarus and Solarus Quest Editor. A lot of issues that you reported on the bug tracker were solved, as well as some problems I detected while working on Return of the Hylian.

- [Download Solarus 1.4](http://www.solarus-games.org/engine/download/ "Download Solarus")
- [Solarus Quest Editor](http://www.solarus-games.org/engine/solarus-quest-editor/)
- [Lua API documentation](http://www.solarus-games.org/doc/latest/lua_api.html "LUA API documentation")
- [Migration guide](http://wiki.solarus-games.org/doku.php?id=migration_guide)

## Changes in Solarus 1.4.3

- Fix crash at exit when a surface has a movement with callback (#699).
- Fix crash when removing a custom entity (#690).
- Fix crash when a sprite file is missing or has no animation (#700).
- Fix crash when trying to remove a sprite already removed (#705).
- Fix crash when a custom entity collision or traversable test errors.
- Fix crash when changing hero sprites sometimes.
- Fix crash when sound buffers are full.
- Fix crash in map:get_ground() with out of bounds coordinates.
- Fix Lua error message saying "number expected" instead of "string expected".
- Fix game:set_command_keyboard/joypad_binding refusing parameters.
- Fix map scrolling not working if quest size is not a multiple of 5 (#701).
- Fix camera:move() ignoring separators.
- Fix entities already destroyed when map:on_finished() is called (#691).
- Fix entity:bring_to_front()/back() ignoring the order of obstacles.
- Fix hero stuck on blocks.
- Fix hero going backwards on ice sometimes.
- Fix custom_entity:set_can_traverse_ground() giving opposite result (#668).
- Fix enemy:immobilize() having no effect when already immobilized.
- Fix dying animation of flying and swimming enemies.
- Fix the position of the shadow of pickables when they move.
- Fix pickables not reacting to their ground (#655).
- Fix a compilation error with Mac OS X.

## Changes in Solarus Quest Editor 1.4.3

- Quest properties editor: fix setting the write directory field empty (#36).
- Sprite editor: add a button to refresh the source image (#50).
- Map editor: fix crash when resizing with no entities selected (#51).
- Map editor: fix entities still visible when adding them on a hidden layer.
- Map editor: fix entity dialog allowing to set illegal sizes (#23).
- Map editor: fix changing the direction of a jumper from the dialog (#60).
- Map editor: fix sprites not always updated when changing direction (#32).
- Map editor: show a context menu when right-clicking an empty space (#26).
- Tileset editor: fix usability issues to create and select patterns (#31).
- Tileset editor: fix moving a pattern to a partially overlapping place (#29).
- Tileset editor: fix color when moving a pattern to an occupied place (#34).
- Tileset editor: fix existing selection lost if selecting with ctrl or shift.
- Text editor: the find button is now the default one in the find dialog (#30).
- Dialogs editor: fix crash when comparing empty dialogs (#48).
- Dialogs editor: ensure that the text ends with a newline (#45).
- Make the numpad enter key work like the return key (#49).
- Check the Solarus library version at compilation time (#41).

Enjoy!
