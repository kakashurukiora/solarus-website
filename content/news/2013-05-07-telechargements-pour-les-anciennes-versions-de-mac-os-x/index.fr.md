---
date: '2013-05-07'
excerpt: Bonjour à tous, Jusqu'à présent, nos jeux sont disponibles pour Mac OS X 10.6 et supérieur, en version 64 bits optimisée pour les machines...
tags:
- solarus
title: Téléchargements pour les anciennes versions de Mac OS X
---

Bonjour à tous,

Jusqu'à présent, nos jeux sont disponibles pour Mac OS X 10.6 et supérieur, en version 64 bits optimisée pour les machines relativement récentes.

Grâce au travail de Lelinuxien, deux nouveaux systèmes sont disponibles en téléchargement pour nos jeux. Il s'agit des anciennes versions de Mac OS X. Plus précisément, vous pouvez maintenant télécharger nos jeux sur les Mac OS X 32 bits (10.4 et supérieur), ainsi que sur les anciens Mac OS X PPC (10.2.8 à 10.3.9).

- [Téléchargements de Mystery of Solarus DX](http://www.zelda-solarus.com/zs/article/zmosdx-telechargements/)
- [Téléchargements de Mystery of Solarus XD](http://www.zelda-solarus.com/zs/article/zmosxd-telechargements/)

Par ailleurs, sachez qu'une nouvelle mise à jour des deux jeux est en cours de finalisation, avec des corrections de bugs et la version allemande de ZSDX.
