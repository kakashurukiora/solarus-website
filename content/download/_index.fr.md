---
title: Téléchargement
excerpt: Téléchargez le moteur de jeu Solarus et ses outils d'aide à la création.
type: singles
layout: download
tags: [download, install, installer, executable, engine, player, windows, macos, mac, linux]
aliases:
  - /fr/engine
  - /fr/downloads
  - /fr/telechargement
---

Selon que vous soyez un [joueur](#solarus-launcher) ou un [développeur de jeux](#solarus-quest-editor), voici les paquets à télécharger.
