---
title: Download
excerpt: Download the Solarus engine and its game-creation tools.
type: singles
layout: download
tags: [download, install, installer, executable, engine, player, windows, macos, mac, linux]
aliases:
  - /engine
  - /downloads
---

Depending on whether you are a [gamer](#solarus-launcher) or a [game developer](#solarus-quest-editor), here are the packages to download.
