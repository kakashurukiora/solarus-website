---
title: Previous Downloads
excerpt: Download links for previous versions of the Solarus engine and its game-creation tools.
tags: [previous]
type: singles
layout: previous-downloads
aliases:
  - /download/old
  - /download/previous
---

Please find here the download links for the previous versions of the Solarus engine and its game creation tools.
