---
title: Windows
downloadPackage: solarus-quest-editor
platform: windows
gitlabProjectId: 6933848
gitlabAssetFileRegex: "solarus-x64-v\\d+.\\d+.\\d+.zip"
version: latest
type: singles
layout: download-package-platform
redirect: /download/solarus-quest-editor/windows/index.json
---
