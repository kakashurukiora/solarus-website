---
excerpt: 'Vue générale du monde extérieur. Vous y trouverez la liste des ennemis de chaque zone ainsi que les lieux accessibles.'
tags: []
title: 'Annexe : Carte du monde'
---

Voici une vue générale du monde extérieur du jeu. Vous y trouverez la liste des ennemis de chaque zone ainsi que les lieux accessibles.

## Mont Terreur Ouest (A1)

![Carte de la zone](a1-west-mount-terror.png)

### Ennemis

- Homme-chien Bleu x3
- Homme-chien Rouge x2

### Lieux

1. Entrée Caverne Ouest RDC
1. Sortie Caverne Ouest 1er Étage (Sortie Ouest)
1. Sortie Caverne Ouest RDC (Sortie Est)
1. Temple de Cristal (Niveau 7)
1. Sortie Caverne Ouest 1er Étage (Sortie Est)
1. Caverne aux Lasers
1. Entrée Caverne Ouest 2nd Étage
1. Sortie Caverne Ouest 2nd Étage

## Mont Terreur Est (B1)

![Carte de la zone](b1-east-mount-terror.png)

### Ennemis

- Homme-chien Bleu x2
- Homme-chien Rouge x2

### Lieux

1. Fontaine des Fées du Nord
1. Sortie Caverne Est 3eme Étage
1. Sortie Caverne Est 4eme Étage
1. Caverne de l'Illusion Optique
1. Sortie de la Caverne de l'Illusion Optique
1. Donjon des Pics Rocheux (Niveau 8)

## Ancien Château (A2)

![Carte de la zone](a2-ancient-castle.png)

### Ennemis

- Chevalier Bleu x2
- Chevalier Rouge x1

### Lieux

1. Entrée Principale de l'Ancien Château (Niveau 5)
1. Entrée des Cachots
1. Entrée Cachette de la Cascade
1. Sortie sur les toits de l'Ancien Château
1. Sortie Aile Ouest de l'Ancien Château
1. Entrée Aile Est de l'Ancien Château
1. Entrée Labyrinthe du Vrai Héros
1. Sortie Labyrinthe du Vrai Héros

## Hutte de la Sorcière (B2)

![Carte de la zone](b2-witch-hut.png)

### Ennemis

- Chevalier Bleu x2
- Chevalier Rouge x1

### Lieux

1. Entrée du Labyrinthe de la Rivière
1. Entrée Ouest du Canal
1. Entrée Est du Canal
1. Hutte de la Sorcière
1. Caverne d'Inferno (Niveau 6)
1. Bazar du Coin
1. Sortie Fontaine des Fées du Village
1. Entrée Fontaine des Fées du Village (1er Étage)
1. Tunnel de la Cascade
1. Entrée Caverne Est RDC
1. Sortie Caverne Est 1er Étage
1. Sortie Caverne Est 2eme Étage
1. Entrée Caverne Est 1er Étage

## Village Ouest (A3)

![Carte de la zone](a3-west-lyriann.png)

### Ennemis

- Soldat x3
- Chevalier Vert x2
- Chevalier Bleu x1
- Chevalier Rouge x1

### Lieux

1. Maison de Link
1. Magasin / Armurerie
1. Maison de Sahasrahla
1. Pâtisserie
1. Cave du Forgeron
1. Caverne du trou fleuri
1. Cabine télépathique
1. Caverne de Roc (Niveau 2)
1. Cave aux bombes
1. Antre de Maître Arbror (Niveau 3)
1. Entrée secrète Antre de Maître Arbror

## Village Est (B3)

![Carte de la zone](b3-east-lyriann.png)

### Ennemis

- Soldat x2
- Chevalier Vert x2
- Chevalier Bleu x1
- Chevalier Rouge x1

### Lieux

1. Maison des Rubis
1. Maison de Mamie Lyly
1. Caverne de Lyriann
1. Fontaine des Fées de Lyriann
1. Cave de la Clé d'Os
1. Cave des Bois Perdus
1. Cave des Coffres
1. Tunnel de la Cascade

## Tour des Cieux (C3)

![Carte de la zone](c3-skyward-tower.png)

### Ennemis

- Ropa x2
- Snapdragon x3

### Lieux

1. Tour des Cieux (Niveau Secret)

## Forêt Sud (A4)

![Carte de la zone](a4-south-forest.png)

### Ennemis

- Soldat x4
- Chevalier Vert x1
- Chevalier Bleu x1

### Lieux

1. Donjon de la Forêt (Niveau 1)
1. Sortie secrète Donjon de la Forêt
1. Caves Jumelles
1. Caves Jumelles
1. Vers le Palais de Beaumont (Niveau 4)
1. Entrée Cave Ouest du Lac
1. Sortie Cave Ouest du Lac
1. Téléporteur vers Temple des Souvenirs (Niveau 9)
1. Sortie ouest 1er étage Temple des Souvenirs
1. Sortie est 1er étage Temple des Souvenirs

## Lac (B4)

![Carte de la zone](b4-lake.png)

### Ennemis

- Chevalier Vert x1
- Chevalier Bleu x1
- Chevalier Rouge x1

### Lieux

1. Boutique du Lac
1. Sortie secrète Boutique du Lac
1. Maison aux Panneaux
1. Repaire de Billy le Téméraire
1. Sortie cachée Grotte sous la Cascade
1. Sortie Grotte sous la Cascade
1. Cave Est du Lac
