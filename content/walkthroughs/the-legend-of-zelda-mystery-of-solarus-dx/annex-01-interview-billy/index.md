---
excerpt: 'An interview of Billy the Reckless, that allows to discover the character more.'
tags: []
title: 'Annex: Interview with Billy the Reckless'
---

Following the misadventures of the famous well-known adventurer Billy against the famous hero of Hyrule, we went to see him in his secret retreat to know a little better the defeated ruffian, the brave fallen, but also the man who lost almost everything during of the event now well known as the Fall of the Rocky Peaks.

**Zelda-Solarus**: Hello Billy and thank you for welcoming us to your new haven of peace. Could you give us a little recap of your career?\
**Billy the Bold**: Hello, it is a pleasure to receive you. I started my career as an official in the 1st Federation of Inventors. My job bored me and I turned bad by becoming a Bandit of Grand Chemin. I worked in the Lost Woods until a smuggler ruined our band. So I converted to become an adventurer, with a good knowledge of the country and more or less useful contacts to achieve some quests. One of my great successes was to find the legendary Edelweiss flower in the Mount Terror. My sponsor passed away before I finished my quest, it stayed in my stuff for some time until someone made a gold price of it. This was my last adventure because Agahnim contacted me shortly thereafter.

**ZS**: Exactly, many of our readers would like to understand your motives, why betray and join Agahnim?\
**BR**: Agahnim is not a simple wizard, he knows many ways to bend wills. In spite of all the gall I have shown in the past, I had honored myself honorably, but Agahnim was able to reopen the wounds of the past and darkness once again took hold of my being. I never tried to hurt the old sage and the children. Sahasrahla would never have trusted me if he had any doubt about my integrity. I deeply regret having been so manipulated by the Red Wizard.

**ZS**: You have decided to withdraw from Hyrule for a while, why this choice that might make people believe that you do not believe yourself in your innocence?\
**BR**: I retired because I prefer to allow time to heal some wounds. The story repeats itself and I'm afraid to go back into the Dark Arts again if Ganon and his minions were to come back one day.

**ZS**: A rumor swells lately in taverns and inns on a very powerful weapon hidden in your dungeon. Do you have any additional information to give us?\
**BR**: I can tell you that there have never been any weapons in the Rock Peaks Dungeon. The only thing one can find there is already taken by the hero of Hyrule and it is about Mirror Shield. However, it is obvious that there are secret passages in my dungeon, because it is common knowledge that I rarely move outdoors: I know all the tunnels and underground of Hyrule, and there is of course an obvious connection between my dungeon and my cave.

**ZS**: And there will be no more clues?\
**BR**: If you were given more clues, it would no longer be a secret passage. Think about it.

**ZS**: All right, what are your passions in life? Outside of the adventure obviously.\
**BR**: I love art so much, that's what pushed me to banditry at first. I particularly like modern art, when shapes and curves are perfectly symmetrical, mathematical. I also really like DIY: I remade myself the tiles of some rooms of the Rock Peaks Dungeon. I also have some other properties or realized some sites that take from a decoration point of view the great monuments of our beautiful country.

**ZS**: Do you have any tips for our frequent readers?\
**BR**: Never trust what you see. Evil distorts reality to reveal the illusion of an obstacle. I knew a wizarding family before Agahnim made his appearance, and they taught me never to trust what I see: sometimes a pillar in a station turns out to be a gateway to another facet of this world. Never give up.

![Hyrule Interior of a Neoclassic Tower](hyrule-castle.png)

**ZS**: There is a book of demonstration of your talents as a handyman/artist, one could even speak of architect at this level. Would you like to share your favorites with our readers?\
**BR**: Of course. I have selected for you six of my best creations. Here is the first, it was before the tragedy fell on the royal family: following the problems encountered with the Evil Lord, the Castle of Hyrule was in a bad state, so there was a will of our good King to renovate the room. I kept the original spirit, but alas the project was rejected because they took this opportunity to expand the entrance, and give what you know now. I believe it is the project of a certain Eve Pnentingon. And I congratulate her again, she has earned her glory.

![Villa In Poetry](beach-house.png)

Here is the villa I had during my trip to the bandits. The room you see there is just the entrance to the house, built on the beach: you open the door and can dive into the sea. A real treat. It was the time when one did not need a building permit to embark on the adventure of the incredible huts. But, to be totally honest, the duke of the region was one of us, so we can say that the license would have been an administrative formality! (_laughter_) Alas, she was destroyed during the Great Storm three years ago. But I hope one day I can do it again, with perhaps some more security.

![Safety Floor Tower](mr-drolomm-room.png)

This one is a project which represented quite a challenge! My client, Mr. Drolomm, an enthusiast of all that was creeping (whether insect, snake, mollusk ...), wanted to create a house where you could jump from one floor to another without risk of hurting yourself. In this image, it is not necessarily seen, but each hole communicates with either a toboggan to slide without getting hurt, or a vertical bar of intervention, like firefighters. The floors are stacked so never to have two holes too close, because a fall of several floors could be fatal and that for the business, it is really not what is better.

![Sewer Bathroom Anti-Mischief](sewer-agent-bathroom.png)

This bathroom was one of the most original to set up. It is a sewer agent who wanted to put it in place at home, because it is a man who feels good only feet in the water, so the whole room is constantly bathed in renewed water (you can notice the renewal in the walls). It's like having a giant fountain at home. And you may not believe me, but the effect of this room is extremely relaxing, we are rocked by a small lapping and thanks to that, there is also a security measure: no one can sneak into this room . What discourage more than one thief. I had this idea shortly after my reconversion as an adventurer. It should be noted that I still left dry areas, especially for storage spaces like the chest in the center, the wood of these boxes poorly supporting the water.

![Johnny's Pipes](pipe-room.png)

This piece was commissioned by a bookmaker, nicknamed Johnny-Good-Tips. A fantastic guy, a pearl, but who did not deserve his nickname ... He wanted a room allowing him to overcome several customers unhappy with his predictions on the Zorathlons. I think he did not foresee that a pipe that has an inlet and an outlet can become a pipe with two entrances and that we can be sandwiched by some hot-blooded people... He died there a year ago, and his house sold at auction. It's a guy a little cold, like with eyes of ice, who has recovered it. He contacted me for details on certain alloys to build the pipes, because he wanted to improve the concept without going through a central rock supporting the structure, but believe me: there is no improvement possible, whatever the metal used!

![Entrance to Ganon Tower](ganon-tower-entrance.png)

And finally, my masterpiece: sober, elegant, and especially symmetrical: the entrance to the Tower of Ganon. He ordered it to me when I was still a bandit, but the fact that my client did these terrible things in this tower can not help but feel a shameful pride in my work. It is so beautiful... I know it's a matter of taste, I redid it in my secret passage, and every time I have a cockroach, I go back. I thought at first changing the statues for a less diabolical model, but the whole thing was becoming incoherent. Sometimes you have to assume your responsibilities. At the time, I was not controlled by a dark sorcerer, so I decided to use this room for my repentance.

**Zelda-Solarus**: Thank you very much for all this information and confidences, and the Zelda-Solarus team wishes you all the best for the next events.\
**Billy the Reckless**: Thank you.

Interview by Renkineko for Solarus-Games and Zelda-Solarus.
