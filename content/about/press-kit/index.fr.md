---
title: Kit de presse
excerpt: Kit de presse pour faciliter la communication sur Solarus.
tags: [press, kit, presskit, toolkit, branding, communication, journaliste, presse]
aliases:
  - /presse
  - /fr/about/press
  - /fr/about/presskit
---

## Formulation

Quand vous présentez Solarus, vous pouvez utiliser la formulation suivante:

> Solarus est un moteur de jeu 2D multiplateforme libre et open-source, écrit in C++. Il est principalement destiné aux Action-RPGs, aussi appelés _Zelda-likes_, mais peut faire bien plus.

Notes:

- Évitez s'il vous plaît de présenter Solarus comme un moteur pour *The Legend of Zelda*™. Il a des fonctionnalités incluses pour Zelda-like pour des raisons historiques, mais vous pouvez faire n'importe quel type de jeu 2D avec.
- Évitez s'il vous plaît de présenter Solarus comme un émulateur de Super Nintendo™ ou un hack de ROM. C'est un moteur écrit à partir de zéro.

## Identité de marque

Préférez toujours les versions avec cadre sur les versions sans cadre.

### Logo

{{< branding-logo background="dark" file="solarus_new_logo_dark" caption="Pour les fonds sombres" >}}

{{< branding-logo background="light" file="solarus_new_logo_white" caption="Pour les fonds clairs" >}}

{{< branding-logo background="dark" file="solarus_new_logo_white_noframe" caption="Pour les fonds sombres" >}}

{{< branding-logo background="light" file="solarus_new_logo_dark_noframe" caption="Pour les fonds clairs" >}}

### Icône

{{< branding-logo background="light" file="solarus_new_logo_mark_frame" caption="Pour les fonds clair" class="small" >}}

{{< branding-logo background="dark" file="solarus_new_logo_mark_white_frame" caption="Pour les fonds sombres" class="small" >}}

{{< branding-logo background="dark" file="solarus_new_logo_mark_noframe" caption="Pour tous les fonds" class="small" >}}

### Couleurs

{{< branding-colors >}}

### Polices de caractères

{{< branding-fonts >}}

## Captures d'écran

### Moteur

![Children of Solarus screenshot](engine/children-of-solarus-1.png)
![Children of Solarus screenshot](engine/children-of-solarus-2.png)
![Children of Solarus screenshot](engine/children-of-solarus-3.png)

### Éditeur

![Solarus Quest Editor - Map Editor](editor/editor-screenshot-map.png)
![Solarus Quest Editor - Entity Editor](editor/editor-screenshot-entity.png)
![Solarus Quest Editor - Dialogs Editor](editor/editor-screenshot-dialogs.png)
![Solarus Quest Editor - Sprite Editor](editor/editor-screenshot-sprite.png)

### Lanceur

À venir.

## Divers

### Fonds d'écran

{{< image file="/img/homepage-artwork-high-res.png" alt="Homepage illustration">}}

{{< image file="/img/homepage-artwork-high-res-color.png" alt="Homepage illustration (color)">}}
