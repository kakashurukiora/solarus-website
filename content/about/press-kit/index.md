---
title: Press Kit
excerpt: Press kit to facilitate communication about Solarus.
tags: [press, kit, presskit, toolkit, branding, communication, journalist, reporter]
aliases:
  - /presskit
  - /press
  - /about/press
  - /about/presskit
  - /en/about/press
  - /en/about/presskit
---

## Wording

When you present Solarus, you may use the following wording:

> Solarus is a multi-platform, free and open-source 2D game engine, written in C++. It is primarily dedicated to Action-RPGs, also called _Zelda-likes_, but can actually do much more.

Notes:

- Please avoid presenting Solarus as a _The Legend of Zelda_ ™ engine. It has built-in features for Zelda-like games for historical reasons, but you can do any kind of 2D game now.
- Please avoid presenting Solarus as Super Nintendo™ emulator or ROM hack. It is an engine buit from scratch.

## Branding

Always prefer the frame versions over the frameless ones.

### Logotype

{{< branding-logo background="dark" file="solarus_new_logo_dark" caption="For dark backgrounds" >}}

{{< branding-logo background="light" file="solarus_new_logo_white" caption="For light backgrounds" >}}

{{< branding-logo background="dark" file="solarus_new_logo_white_noframe" caption="For dark backgrounds" >}}

{{< branding-logo background="light" file="solarus_new_logo_dark_noframe" caption="For white backgrounds" >}}

### Mark

{{< branding-logo background="light" file="solarus_new_logo_mark_frame" caption="For white backgrounds" class="small" >}}

{{< branding-logo background="dark" file="solarus_new_logo_mark_white_frame" caption="For dark backgrounds" class="small" >}}

{{< branding-logo background="dark" file="solarus_new_logo_mark_noframe" caption="For all backgrounds" class="small" >}}

### Colors

{{< branding-colors >}}

### Fonts

{{< branding-fonts >}}

## Product screenshots

### Engine

{{< image file="engine/children-of-solarus-1.png" alt="Children of Solarus screenshot">}}

{{< image file="engine/children-of-solarus-2.png" alt="Children of Solarus screenshot">}}

{{< image file="engine/children-of-solarus-3.png" alt="Children of Solarus screenshot">}}

### Editor

{{< image file="editor/editor-screenshot-map.png" alt="Solarus Quest Editor - Map Editor">}}

{{< image file="editor/editor-screenshot-entity.png" alt="Solarus Quest Editor - Entity Editor">}}

{{< image file="editor/editor-screenshot-dialogs.png" alt="Solarus Quest Editor - Dialog Editor">}}

{{< image file="editor/editor-screenshot-sprite.png" alt="Solarus Quest Editor - Sprite Editor">}}

### Launcher

Coming soon.

## Misc

### Wallpapers

{{< image file="/img/homepage-artwork-high-res.png" alt="Homepage illustration">}}

{{< image file="/img/homepage-artwork-high-res-color.png" alt="Homepage illustration (color)">}}
