---
title: Foire aux questions
excerpt: Questions fréquemment posées à propos du moteur de jeu Solarus.
tags: [faq, commercial game, dmca, contact, team]
aliases:
  - /fr/faq
  - /fr/frequently-asked-questions
  - /fr/about/frequently-asked-questions
---

## À propos de Solarus

### Qu'est-ce que Solarus?

Solarus est un **moteur de jeu gratuit et open-source**, sous licence GPL v3. Il est **écrit à partir de zéro en C++** et utilise SDL2. Par conséquent, il est disponible sur un grand nombre de machines. Le développement a commencé en 2006. Le but du moteur est de **permettre aux gens de créer leurs propres jeux Action-RPG** (jeux également appelés Zelda-like). Il est totalement indépendant de Nintendo.

- **Solarus n'est pas un émulateur Super Nintendo™**. C'est un véritable moteur de jeu écrit à partir de zéro.
- **Solarus n'est pas un hack de ROM Super Nintendo™**. Il ne fonctionne pas sur SNES, ni sur les émulateurs SNES.
- **Solarus n'est pas un "moteur Zelda"** comme on peut le lire parfois. Il possède des fonctionnalités intégrées pour tout jeu d'Action-RPG ou d'Action-Aventure, et permet également de créer n'importe quel type de jeu.

### Qu'est-il possible de faire avec Solarus ?

On nous pose souvent des questions tel que "Puis-je faire ceci ou cela avec Solarus ?". La réponse est probablement **oui**, vous pouvez le faire.

Solarus a une API Lua, et vous pouvez programmer ce que vous voulez en Lua. Par conséquent, faire fonctionner quelque chose dans Solarus est presque toujours possible si vous êtes prêt à vous mettre au travail. Cependant, le moteur ne gère pas les graphismes 3D : c'est la seule limitation. Tout type de jeu est possible.

### Solarus pourra-t-il un jour recevoir un DMCA (ou EUCD en Union Européenne) ?

Non, car Solarus ne contient aucun code ou ressource protégée par le droit d'auteur : il est 100 % libre et open-source. Ainsi, Solarus ne peut pas être éligible à un retrait _Digital Millennium Copyright Act_ de la part de quiconque, y compris Nintendo, si c'est ce à quoi vous pensiez.

## À propos des licences

### Puis-je utiliser Solarus pour un jeu commercial ?

**Oui, vous pouvez**, et cela a déjà été fait.

Tout d'abord, sachez que votre jeu sera divisé en 2 parties :

- Le moteur (Solarus).
- Les données du jeu (maps, scripts, sprites, dialogues, etc.).

**Le moteur Solarus est GPL**, il nécessite donc de suivre certaines règles :

- Si vous utilisez Solarus tel quel : veuillez fournir un lien vers le code source de Solarus, par exemple dans un écran de titre au lancement de votre jeu. De plus, nous apprécierons beaucoup l'affichage du logo Solarus au lancement de votre jeu.
- Si vous modifiez Solarus et publiez publiquement votre jeu, vous devez donner accès à votre code source Solarus modifié. De plus, cette version modifiée est également automatiquement sous licence GPL.

**Vos données de jeu peuvent avoir la licence que vous souhaitez**. Elle peut être différente de GPL et rester un code/des ressources privées. Les licences commerciales sont ainsi autorisées pour votre quête.

- Les tout nouveaux scripts/ressources créées par vous peuvent être sous la licence de votre choix.
- Les scripts/ressources réutilisées ou modifiées doivent respecter les règles de leur licence d'origine. Veuillez faire attention aux scripts GPL Lua ou aux resources CC : ils doivent également être GPL s'ils sont modifiés. Notez également que l'utilisation de ressources propriétaires est interdite par la loi.

Un [chapitre du tutoriel](/fr/development/tutorials/solarus-official-guide/basics/choosing-a-license) est dédié à cette question et donne davantage de détails.

### Puis-je utiliser les sprites, personnages et musiques de Zelda ?

**Pour un jeu commercial, évidemment : NON.** N'utilisez pas d'éléments dont le droit d'auteur appartient à quelqu'un d'autre, y compris Nintendo (sprites, personnages, musique, scénario…). Ou vous vous exposez à des problèmes juridiques. Vous ne pouvez pas dire que nous ne vous avons pas prévenu(e).

**Pour un jeu non commercial**, oui mais tant que cela demeure à petite échelle. Ceci est ce que nous faisons. À moins que Nintendo ne vous dise d'arrêter, cela semble possible. Nintendo semble tolérer les créations faites par des fans à but non lucratif lorsqu'elles ne deviennent pas trop grosses et n'empêchent pas leur propre entreprise, ce qui est tout à fait juste et logique.

Nous pensons que l'utilisation des ressources _Zelda_ comme base pour apprendre le développement de jeux est **une utilisation équitable**. Cependant, vous devez remplacer progressivement ces ressources protégées par des droits d'auteur par des ressources personnalisées libres, afin de ne pas violer le droit d'auteur.

## À propos du projet

### Comment puis-je contribuer au projet ?

Merci! Voir la [page de contribution](/fr/about/contribute).

### Le projet a-t-il un cadre légal ?

Oui, la personne morale qui porte le projet est l'association Loi 1901 **Solarus Labs**. C'est une organisation à but non lucratif. L'association recevra tous les dons faits au projet, et ces dons seront entièrement utilisés pour le projet et uniquement pour cela.

### Comment puis-je contacter l'équipe derrière Solarus ?

Si vous avez une demande/question qui n'est pas ici et que vous souhaitez une réponse plus rapide :

- [Les forums](http://forum.solarus-games.org/)
- [Le chat Discord](https://discord.gg/yYHjJHt)

### Qui se cache derrière Solarus ?

Une petite équipe de contributeurs bénévoles autour de [Christopho](https://twitter.com/ChristophoZS), le créateur de Solarus, entretient ce projet avec amour et passion.
