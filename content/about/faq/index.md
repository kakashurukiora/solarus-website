---
title: Frequently Asked Questions
excerpt: Frequently Asked Questions about the Solarus game engine project.
tags: [faq, commercial game, dmca, contact, team]
aliases:
  - /faq
  - /frequently-asked-questions
  - /about/frequently-asked-questions
---

## About Solarus

### What is Solarus?

Solarus is a **free and open-source game engine**, licensed under GPL v3. It is **written from scratch in C++** and uses SDL2. Consequently, it is available on a large set of machines. Development began in 2006. The goal of the engine is to **allow people to create their own Action-RPG games** (also called Zelda-like games). It is totally independent from Nintendo.

- **Solarus is not an Super Nintendo™ emulator**. It's an actual game engine written from scratch.
- **Solarus is not a Super Nintendo™ ROM hack**. It does not run on SNES, neither on SNES emulators.
- **Solarus is not a "Zelda engine"** as can be read sometimes. It has built-in features for any Action-RPG or Action-Adventure game, and allows to make any kind of game too.

### What is possible to do with Solarus?

We're often asked questions like 'Can I do this with Solarus?'. The answer is probably **yes**, you can.

Solarus has a Lua API, and you can program whatever you want in Lua. Consequently, getting something to work in Solarus is almost always possible if you are willing to put in the work. However, the engine does not handle 3D graphics: this is the only limitation. Any kind of game is possible.

### May Solarus receive a DMCA someday?

No, because Solarus does not contain any copyrighted code or assets: it is 100% free and open-source. Thus, Solarus cannot be eligible for a _Digital Millennium Copyright Act_ takedown from anyone, including Nintendo, if this is what you were thinking about.

## About licenses

### Can I use Solarus for a commercial game?

**Yes, you can**, and it's already been done.

First, please know that your game will be divided into 2 parts:

- The engine (Solarus).
- The game data (maps, scripts, sprites, dialogs, etc.).

**The Solarus engine is GPL**, so it requires to follow some rules:

- If you use Solarus as-is: please provide a link to Solarus source code, for example in a title sceen at the launch of your game. Also, displaying the Solarus logo when your game launches is very much appreciated.
- If you modify Solarus and release publicly your game, you must provide access to your modified Solarus source code. Moreover, this modified version is automatically licensed under GPL too.

**You game data may have whatever license you want**. It may be different than GPL and remain private code/assets. Commercial licenses are thus allowed for your quest.

- Brand-new assets/scripts made by you can be whatever license you want.
- Reused or modified assets/scripts must follow the rules of their original license. Please take care for GPL Lua scripts or CC assets: they must also be GPL if modified. Note also that using proprietary assets is forbidden by law.

A [tutorial chapter](/en/development/tutorials/solarus-official-guide/basics/choosing-a-license) is dedicated to this question and gives more details.

### Can I use Zelda sprites, characters and musics?

**For a commercial game, obviously: NO.** Don't use assets that are copyrighted by anyone else, including Nintendo (sprites, characters, music, storyline…). Or you expose yourself to legal problems. You cannot say we didn't warn you.

**For a non-commercial game**, yes but keep it small. This is what we do. Unless Nintendo tells you to stop, this seems possible. Nintendo seems to tolerate non profit fan-made creations when they don't get too big and prevent their own business, which is totally fair and logical.

We believe using _Zelda_ assets as a basis to learn game dev is **fair use**. However, you should replace progressively these copyrighted assets with custom free ones, in order to not violate copyright.

## About the project

### How can I contribute to the project?

Thank you! See the [contribution page](/about/contribute).

### Is the project backed by a legal entity?

Yes, the legal entity that backs the project is the French Loi 1901 association **Solarus Labs**. It is a nonprofit organization. The association will receive all the donations made to the project, and these donations will be entirely used for the project and only for that.

### How can I contact Solarus Team?

If you have a request/question that is not here, and want a quicker answer:

- [The forums](http://forum.solarus-games.org/)
- [The Discord chat](https://discord.gg/yYHjJHt)

### Who is behind Solarus?

A small team of benevolent contributors around [Christopho](https://twitter.com/ChristophoZS), Solarus' creator, is maintaining this project with passion and love.
