---
title: Contact
excerpt: Comment contacter les personnes derrière le projet de moteur de jeu Solarus.
tags: [contact]
aliases:
  - /fr/contact
---

## Contacter la communauté

Avant de nous contactez, cherchez s'il-vous-plait si votre question ne serait pas déjà répondue dans la page de la [Foire Aux Questions](/fr/about/faq) !

Le [Forum Solarus]({{< param "forumsFrURL" >}}) et le [tchat Discord de Solarus]({{< param "social.discord" >}}) sont les lieux de discussion recommandés. Vouz aurez probablement de meilleures réponses qu'ici car toute la communauté Solarus sera là pour vous répondre. Si vous voulez davantage d'informations, proposer une suggestion our une contribution, ce sont les bons endroits.

## Nous contacter

Cependant, vous pouvez aussi nous contacter directement ici si vous avez une question bien spécifique ou si vous désirer nous contacter en privé.

{{< email-obfuscator >}}
