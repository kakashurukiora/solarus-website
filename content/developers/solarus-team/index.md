---
title: Solarus Team
excerpt: The team behind the Solarus engine. They made games to showcase the engine's possibilities. The team formed back in 2002 around Christopho and his website zelda-solarus.com, people progressively came and joined.
thumbnail: thumbnail.png
website: https://www.solarus-games.org
---
