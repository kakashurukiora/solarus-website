---
age: all
controls:
  - keyboard
  - gamepad
developer: Zeldaforce
excerpt: A SNES-like enhanced remake of the Game Boy cult classic Link's Awakening.
genre:
  - Action-RPG
  - Adventure
id: alttd
languages:
  - en
  - fr
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1
minimumPlayers: 1
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
solarusVersion: 1.6.x
sourceCode: https://gitlab.com/zeldaforce/zelda-alttd
thumbnail: thumbnail.png
title: 'The Legend of Zelda: A Link to the Dream'
website: https://zeldaforce.net/jeux-amateurs/a-link-to-the-dream
---

### Presentation

_The Legend of Zelda: A Link to the Dream_ is a a remake of the Game Boy cult classic _The Legend of Zelda: Link's Awakening_, first published in 1993 and enhanced with colors in 1998 with its _DX_ version.

The Solarus-made remake uses _A Link to the Past_ graphics, extended with lots of custom sprites and tiles, in order to represent all the game's characters, enemies and landscapes. Even the music has been remade in SNES-like fashion, to get the perfect feeling as if the game was released on Super Nintendo.

_A Link to the Dream_ is the ultimate love letter to the legendary pocket adventure. Koholint has never been such a joy to explore!

![The Owl](artwork_owl.png)

### Synopsis

Link was navigating on his boat when suddenly, a huge storm happened. Thunder was rumbling and the ocean was raging. A thunderbolt broke the mast, and destroyed the boat. Link lost consciousness.

He ended up on the beach of an unknown island, still alive but fainted and wounded. A young girl named Marin took him in and looked after him until he finally woke up. She explained him that he was on Kohlint island, a tropical island in the middle of the ocean. Link also soon met Tarin, her father, who found Link's shield on the beach.

Who are these people? Where is Link? And why is this island both so strange and so familiar? And what is this giant egg at the top of the mountain? You'll discover the truth by guiding Link in this epic and oniric quest.

![Marin](artwork_marin.png)
