---
age: all
controls:
  - keyboard
  - gamepad
developer:
  - Solarus Team
  - Vincent Jouillat
download: '6933862'
downloadType: gitlab_api
excerpt: "A Solarus-made remake of Vincent Jouillat's first game, and first episode of a trilogy."
genre:
  - Action-RPG
  - Adventure
id: zelda_roth_se
languages:
  - en
  - fr
  - es
  - de
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2015-08-13
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
solarusVersion: 1.6.x
sourceCode: https://gitlab.com/solarus-games/games/zelda-roth-se'
thumbnail: thumbnail.png
title: 'The Legend of Zelda: Return of the Hylian SE'
version: 1.2.1
website: http://www.zeldaroth.fr/zroth.php
---

### Presentation

_The Legend of Zelda: Return of the Hylian SE_ is a Solarus-made remake of Vincent Jouillat's first game, hence the _SE_ suffix (stands for _Solarus Edition_). It was first released on 12th August, 2006, and made with another custom engine in C++. It has been remade entirely in Solarus to take advantage of the better capabilities of the engine.

### Synopsis

After Link's victory over Ganon in _A Link to the Past_, no one knows what Link’s wish to the Triforce was. But this wish reunified the Light World and the Dark World and brought the Seven Wise Men’s descendants back to life. Peace was back in Hyrule. But unfortunately, this wish also ressurected Ganon and his henchmen. He was preparing his revenge, but he couldn’t do anything without the Triforce.

One night, a familiar voice speaks to Link in his sleep...

![Link](artwork_link.png)
