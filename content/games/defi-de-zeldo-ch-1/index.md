---
age: all
controls:
  - keyboard
  - gamepad
developer: ZeldoRetro
download: ZeldoRetro/defi_zeldo_chap_1
downloadType: github_api
excerpt: Zeldo, a Creator, challenges Link in a dungeon he made... Will Link succeed in Zeldo's Challenge?
genre:
  - Action-RPG
  - Adventure
id: defi_zeldo_chap_1
languages:
  - en
  - fr
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2019-05-10
latestUpdateDate: 2019-09-26
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
solarusVersion: 1.6.x
sourceCode: https://github.com/ZeldoRetro/defi_zeldo_chap_1
thumbnail: thumbnail.png
title: "Zeldo's Challenge Ch.&nbsp;1: Bingo's Revenge"
version: 1.2.1
website: https://www.solarus-games.org/games/defi-de-zeldo-ch-1
---

ZeldoRetro made a short _The Legend of Zelda_ game in 2017 to challenge his friend Adenothe. It was not aimed to be publicly released. However, the release of the second chapter made him consider releasing the first one, because everyone was asking "Wait? If this is chapter 2, where is chapter one?". Here it is !

This game is then the first entry in ZeldoRetro's series _Le Défi de Zeldo_ (Zeldo's Challenge). This first chapter is named _La Revanche du Bingo_ (Bingo's Revenge), and is much less ambitious than its sequel. It contains only one dungeon, and no overworld to explore, which makes the game rather short. You may beat it under an hour.
