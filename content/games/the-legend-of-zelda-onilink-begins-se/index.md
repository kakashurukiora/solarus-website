---
age: all
controls:
  - keyboard
  - gamepad
developer:
  - Solarus Team
  - Vincent Jouillat
excerpt: The remake of Vincent Jouillat's second game. Richer, bigger and harder than the first one.
genre:
  - Action-RPG
  - Adventure
id: zelda_olb_se
languages:
  - en
  - fr
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1
minimumPlayers: 1
screenshots:
  - screen1.jpg
  - screen2.jpg
  - screen3.jpg
  - screen4.jpg
solarusVersion: 1.6.x
sourceCode: https://gitlab.com/solarus-games/zelda-mercuris-chest
thumbnail: thumbnail.png
title: 'The Legend of Zelda: Onilink Begins SE'
website: http://www.zeldaroth.fr/zolb.php
---

### Presentation

_The Legend of Zelda: Onilink Begins SE_ is a Solarus-made remake of Vincent Jouillat's second game, hence the _SE_ suffix (stands for _Solarus Edition_). It was first released on 12th August, 2007, and made with another custom engine in C++. It has been remade entirely in Solarus to take advantage of the better capabilities of the engine.

### Synopsis

Brought down by a terrible curse since his recent victory on the Dark Lord, Link is changing, day by day, into a powerful creature with a destructive nature named Oni-Link. Banished from Hyrule, the young hylian asks the Princess Zelda some help. She shows him his last hope: a portal to a secret world.

![Link](artwork_link.png)
