---
age: all
controls:
  - keyboard
  - gamepad
developer: Moth Atlas
download: https://maxatrillionator.itch.io/hallows-eve
downloadType: itch.io
excerpt: Explore a 90s town as a pumpkin spirit to kick ghosts and rent movies.
genre:
  - Action-RPG
  - Adventure
  - Comedy
id: hallows-eve-saves
languages: ['en']
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2020-10-24
latestUpdateDate: 2020-10-26
platforms:
  - MacOS
  - Windows
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
  - screen5.png
  - screen6.png
  - screen7.png
  - screen8.png
solarusVersion: 1.6.4
sourceCode: https://gitlab.com/maxmraz/hallows-eve-open-code
thumbnail: thumbnail.png
title: "Hallow's Eve"
trailer: uEGRmiMhKn0
version: 3
website: https://maxatrillionator.itch.io/hallows-eve
---

### It's Halloween. It's the 90s. You're a Pumpkin Man.

On Halloween, the barrier between human world and the spirit world thins. The spirits play tricks, the dead can be seen, and monsters can run their human world errands.

Set in the 90s, play as Ichabod, a pumpkin man who uses his chance to enter the human world to try and rent Jurassic Park from the video store. Unfortunately, the only copy has been rented by a teen who's brought a witch's curse upon the town.

Gameplay is inspired by the classics and the modern classics — a Zeldalike sense of exploration, with the tight, fast combat of Hyper Light Drifter. All soaked in a charming pixel art atmosphere and a dumb sense of humor. Playtime is around 1-2 hours.

### Default Controls

|                         Key                         | Action                          |
| :-------------------------------------------------: | ------------------------------- |
| <kbd>←</kbd> <kbd>↑</kbd> <kbd>→</kbd> <kbd>↓</kbd> | Move                            |
|                  <kbd>Space</kbd>                   | Action / Dodge                  |
|                    <kbd>C</kbd>                     | Kick                            |
|                    <kbd>X</kbd>                     | Special Attack                  |
|                    <kbd>V</kbd>                     | Heal                            |
|                    <kbd>D</kbd>                     | Pause / Map                     |
|          <kbd>D</kbd></br>(In Pause Menu)           | Save and return to title screen |
|                    <kbd>F1</kbd>                    | Control Mapping                 |
|                    <kbd>F2</kbd>                    | Options                         |
