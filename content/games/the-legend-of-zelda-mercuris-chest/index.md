---
age: all
controls:
  - keyboard
  - gamepad
developer: Solarus Team
download: '44818747'
downloadType: gitlab_api
excerpt: The ambitious second original game by Solarus Team.<br>A huge overworld for a huge adventure.
genre:
  - Action-RPG
  - Adventure
id: zelda-mercuris-chest-demo
languages:
  - en
  - fr
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1
minimumPlayers: 1
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
  - screen5.png
  - screen6.png
  - screen7.png
  - screen8.png
solarusVersion: 1.6.x
sourceCode: https://gitlab.com/solarus-games/games/zelda-mercuris-chest-demo
thumbnail: thumbnail.png
title: "The Legend of Zelda: Mercuris' Chest"
trailer: SYw8QJ7DW6E
version: 0.2.0
aliases:
  - /zmc
website: https://www.solarus-games.org/games/the-legend-of-zelda-mercuris-chest
---

### Presentation

Every game studio has its vaporware, and _The Legend of Zelda: Mercuris Chest_ is ours! The game has been in development since 2003. A demo, made with Multimedia Fusion, was released in 2003, with a huge, long and intricate dungeon. However, development was cancelled in 2007... only to be remade from scratch in 2013, with Solarus this time!

This is Solarus Team's most ambitious game, and it intends to be the definitive 2D Zelda-like game. The overworld is a huge open-world kingdom, with lots of different landscapes, lots of NPCs and quests. The dungeons should be as huge as the one in the demo, and better designed than in _Mystery of Solarus DX_. Custom sprites (PNJ, enemies, bosses), tilesets and elements have been created to give a special charm the to the game.
Moreover, the story is original, and more developped than the one in _Mystery of Solarus_.

Interestingly, a lot of the ideas we had in 2003 have been used in official _Zelda_ games since. This reinforces that we had good ideas that were in line with the _Zelda_ series.

![Link](artwork_link.png)

### Synopsis

Link was quietly taking a nap in his house, when suddenly 2 strangers kidnapped him and brought him to an unknown kingdom. When he finally woke up, a priest explained that they need Link's help to fight against a giant bird who tries to steal a legendary chest, containing a dangerous weapon. What's in this chest? And who is that mysterious bird warrior? And why this kingdom seems so familiar, but also so different? You will discover everything by playing _Mercuris' Chest_!

![Mercuris](artwork_mercuris.png)
