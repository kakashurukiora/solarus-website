---
age: all
controls:
  - keyboard
  - gamepad
developer: Moth Atlas
download: https://www.nordcurrent.com/games/oceans-heart
downloadType: Other
excerpt: Navigate a world brimming with lawless pirates, dangerous monsters, and ancient magic.
genre:
  - Action-RPG
  - Adventure
id: oceans_heart_saves
languages:
  - en
  - fr
  - de
  - pt
  - es
  - ru
  - zh
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2021-01-21
latestUpdateDate: 2021-02-14
platforms:
  - Windows
  - Nintendo Switch
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
  - screen5.png
  - screen6.png
  - screen7.png
  - screen8.png
  - screen9.png
  - screen10.png
  - screen11.png
  - screen12.png
solarusVersion: 1.6.4
thumbnail: thumbnail.png
title: Ocean's Heart
trailer: spOdb7KyNsE
version: 1.0.1
website: https://mothatlas.co/games/oceans-heart
---

When pirates attacked Limestone Island, it fell to Tilia's father to go after them and rescue the young woman they kidnapped. When he hasn't returned six months later, Tilia decides it's up to her to track down her dad and find out what went wrong.

Play as Tilia as she travels across an archipelago full of pirate forts, forgotten magic, abominable beasts, and a diverse cast of people trying to find their place in it all. As Tilia follows her dad's trail, she becomes entrangled in the pirates' plot to use an old magic to control the ocean. And maybe along the way, she'll play in a tick-tack-toe championship, prevent an art heist, or impresonate a piano tuner. It's a large world full of secrets, dangers, and quirky wierdos for you to explore.

### Exploration

The north sea's waves break against dozens of islands. To finish your quest, you'll need to explore deep forests, saltwater marshland, rocky mountains, vibrant cities, and many other areas. In addition to six large dungeons, the islands are full of mini-dungeons and unique side areas.

By following the world's side-trails, twisting tunnels, and wandering off the road, you'll find secret quests, hidden items, new abilities, and maybe learn about the archipalago's tragic past. Large areas of the world are availble to explore soon after setting out on your adventure, and as Tilia learns new abilities, the world will open up even more.

### Combat

While the islands of the north sea are beautiful, twisted monsters lurk in their dark places. While exploring old ruins, deep caves, or wild hillsides, you'll come up against dozens of different beasts that spread corruption. They can quickly send you to a game over screen, but preparation and quick thinking will keep Tilia alive to later fight lawless pirates.

While hunting for Tilia's father and following side quests, you'll come across around one hundred different kinds of enemies to overcome. Besides the wide variety of monsters and pirates roaming the world, there are many unique creatures waiting to give you a fight if you can find them.

### Growth

If Tilia is going to finish her quest, she's going to need to grow to be a strong and resourceful woman. While searching the world's mossy ruins and pirate shipwrecks, you'll find both ways to increase Tilia's strength and many items and magic abilities that will help you to fight and explore. Many of the tools you'll come across have utility both inside and outside of combat. And while you can expect to find useful items while exploring the game's dungeons, exploring out of the way locations and following side quests will be responsible for filling out much of your inventory.

But Tilia can only get so far using tools others have made for her. By combining plants you can forage for and monster parts dropped in battle, Tilia can craft potions to make her deadlier and healthier.

### Stories

Pirates have been forming aliances and growing their power. Deadly monsters have been seen lurking in forests and caverns. The mayor is throwing a birthday party for his dog. As there are many people making their lives on the islands, there are many stories. Tilia will find herself involved in many quests, from the epic to the petty. Quests are rarely straightforward, and occasionally branch off, or invite a nonlinear approach. It's up to you- you can follow after Tilia's dad as quickly as possible by yourself, or learn about the people of the world, involve yourself in their stories, and get help from them.
