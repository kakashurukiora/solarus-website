---
age: all
controls:
  - keyboard
  - gamepad
developer: Solarus Team
download: '6933864'
downloadType: gitlab_api
excerpt: The sequel of the first XD short parodic game. Bigger. Better. Sillier.
genre:
  - Action-RPG
  - Adventure
id: zelda-xd2-mercuris-chess
languages:
  - en
  - fr
  - es
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2017-04-01
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
trailer: M2tAk5hRMHk
solarusVersion: 1.6.x
sourceCode: https://gitlab.com/solarus-games/games/zelda-xd2-mercuris-chess
thumbnail: thumbnail.png
title: 'The Legend of Zelda XD2: Mercuris Chess'
version: 1.1.2
walkthroughs:
  - type: video
    url: https://www.youtube.com/watch?v=59rctsH8-6c&list=PLzJ4jb-Y0ufzCmtv8EA0-PpIfKqHDSfG2
website: https://www.solarus-games.org/games/the-legend-of-zelda-xd2-mercuris-chess
---

### Presentation

_The Legend of Zelda XD2: Mercuris Chess_ is the sequel of the first _XD_ parodic game, and was also released on April 1st, in 2017 this time. As a direct sequel, the overworld is expanded, the story is extended and the funny tone is kept, if not bettered!

The game was made in 10 weeks (!), beginning with an a discussion about how people always mispronounce _Mercuris Chest_ (note the T at the end). Diarandor suggested that a game could be named _Mercuris Chess_ to make fun of the title, and the same day it was decided that it would be the next big April’s fools joke.

The team worked really hard, and you can feel the improvements from the previous games. The game has 2 dungeons, and lots of side-quests and secrets everywhere, and also feature silly NPCs around the overworld. Be sure to speak to every one to not miss a joke!

![Link](artwork_link.png)

### Synopsis

You do not need to have played the first _XD_ game to enjoy _Mercuris Chess_. However, to appreciate the game’s introduction, you need to know that, in the first episode, Link locked Zelda up in his cave after a drinking night.

The story takes place some months after that terrible night. While Link was trying to make up for his fault, a very rich business man named Mr Grump took over the kingdom and bought everything.

There begins your next adventure, in a world where everything has a price and where truth and lies become undistinguishable.

![M. Grump](artwork_grump.png)
