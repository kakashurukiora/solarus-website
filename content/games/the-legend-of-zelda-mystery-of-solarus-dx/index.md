---
age: all
controls:
  - keyboard
  - gamepad
developer: Solarus Team
download: '6933865'
downloadType: gitlab_api
excerpt: The sequel to A Link to the Past, and the first game made with Solarus.
genre:
  - Action-RPG
  - Adventure
id: zsdx
languages:
  - en
  - fr
  - it
  - es
  - de
  - zh
licenses:
  - GPL v3
  - CC-BY-SA 4.0
  - Proprietary (Fair use)
maximumPlayers: 1
minimumPlayers: 1
initialReleaseDate: 2011-12-16
screenshots:
  - screen1.png
  - screen2.png
  - screen3.png
  - screen4.png
  - screen5.png
  - screen6.png
  - screen7.png
  - screen8.png
solarusVersion: 1.6.x
sourceCode: https://gitlab.com/solarus-games/games/zsdx
thumbnail: 'thumbnail.png'
title: 'The Legend of Zelda: Mystery of Solarus DX'
trailer: BUxREyXILLs
version: 1.12.3
walkthroughs:
  - type: walkthrough
  - type: video
    url: https://www.youtube.com/watch?v=CY1z5BGBSq0&list=PLzJ4jb-Y0ufyvGrqfDpotZHUvZrcswjGR
website: https://www.solarus-games.org/games/the-legend-of-zelda-mystery-of-solarus-dx
---

### The sequel to _A Link to the Past_

**_The Legend of Zelda: Mystery of Solarus DX_** is set to be a direct sequel to _The Legend of Zelda: A Link to the Past_ on SNES, using the same graphics and game mechanisms. _Mystery of Solarus DX_ was the first game made with the Solarus engine and in fact, Solarus was primarily designed for this game.

![Link](artwork_link.png)

### Origins

_Mystery of Solarus DX_ is actually an enhanced remake of a first creation, _Mystery of Solarus_ (without the _DX_ suffix). This first creation, developed with RPG Maker 2000, was released in 2002 and was only available in French. The _DX_ project was unveiled on April 1st, 2008. Its objectives were to correct the many flaws of its predecessor: the battle system, the bosses, the use of items, etc.

However, that is hardly all of it, as new graphical elements and musics have been created to accompany you throughout the game. This _Deluxe_ version is the opportunity for you to relive the original adventure in a brand new way, or to discover it for the first time if you've never played it before!

![Ganon](artwork_ganon.png)

### Synopsis

In _A Link to the Past_, Ganon was defeated and forever imprisonned in the Golden Land by Hyrule's King, thanks to the Seal of the Seven Sages.

Years passed, and someday the King became affected by a strange and grave illness. Doctors and apothecaries from across the kingdom tried in vain to cure him. The king passed away, and his power vanished, weakening the Seal of the Seven Sages.

The Hero, under the guidance of his master Sahasrahla, trusted the heiress of the royal powers, Princess Zelda, with the Triforce. Zelda teamed with eight mysterious children to shatter the Triforce into eight fragments and conceal them across the kingdom. Peace was restored.

And so begins your journey, full of monsters, mysterious characters, labyrinths and puzzles...

![Princess Zelda](artwork_zelda.png)
