'use strict';

document.addEventListener('DOMContentLoaded', () => {
  // Check if article and aside are present.
  const article = document.querySelector('article');
  if (!article) return;
  const aside = document.getElementById('table-of-contents-aside');
  if (!aside) return;

  // Node {
  //   level: number
  //   element: HTMLElement
  //   link: HTMLElement
  //   top: number
  //   bottom: number
  //   parents: Node[]
  // }
  let nodesCache = [];

  function getParent(node) {
    const nodeIndex = nodesCache.indexOf(node);
    if (nodeIndex >= 0 && nodeIndex < nodesCache.length) {
      for (let i = nodeIndex - 1; i >= 0; i--) {
        const otherNode = nodesCache[i];
        if (otherNode.level < node.level) {
          return otherNode;
        }
      }
    }

    return undefined;
  }

  function getParents(node) {
    let parent = node;
    const parents = [];
    do {
      const parentParent = getParent(parent);
      if (parentParent !== undefined) {
        parents.push(parentParent);
      }
      parent = parentParent;
    } while (parent !== undefined);

    return parents;
  }

  function updateNodesCache() {
    const titles = document.querySelectorAll('h2[id],h3[id],h4[id]');
    nodesCache = [];
    for (let i = 0; i < titles.length; i++) {
      const title = titles[i];
      const match = title.tagName.match(/H([0-9]+)/);
      if (match.length > 0) {
        // Find element's top and bottom.
        const level = parseInt(match[1]);
        const top = title.offsetTop;
        const node = {
          element: title,
          level: level,
          top: top,
          bottom: 0,
        };
        nodesCache.push(node);
        const previous = i - 1;
        if (previous >= 0) {
          nodesCache[previous].bottom = node.top;
        }

        // Tricks to ensure first and last are selected.
        if (i == 0) {
          node.top = 0;
        }
        if (i == titles.length - 1) {
          node.bottom = document.body.scrollHeight;
        }

        // Find corresponding link.
        const titleId = title.id;
        const link = document.querySelector(`aside nav li a[href="#${titleId}"]`);
        node.link = link;

        // Find parents.
        node.parents = getParents(node);
      }
    }
  }

  const TOC_ACTIVE_CSS_CLASS = 'current';

  function highlightNode(node, isHighlighted) {
    if (isHighlighted) {
      // Highlight current item.
      node.link.classList.add(TOC_ACTIVE_CSS_CLASS);

      // Also highlight item's parents.
      node.parents.forEach((parent) => parent.link.classList.add(TOC_ACTIVE_CSS_CLASS));
    } else {
      node.link.classList.remove(TOC_ACTIVE_CSS_CLASS);
    }
  }

  function updateHighlightedNode() {
    if (nodesCache.length == 0) return;

    // First, remove all highlights to be sure.
    for (const node of nodesCache) {
      highlightNode(node, false);
    }

    // We modify the scroll to cheat a bit: the eye looks at 1/3 of the screen.
    const scroll = window.scrollY;
    const modifiedScroll = Math.max(0, scroll + 300);

    // Then, find the current one.
    for (const node of nodesCache) {
      // Handle special cases.
      if (scroll === 0) {
        // Always highlight the first one when scroll is at the top.
        highlightNode(nodesCache[0], true);
        return;
      } else if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        // Always highlight the last one when scroll is at the bottom.
        highlightNode(nodesCache[nodesCache.length - 1], true);
        return;
      }

      // Check if scroll is within boundaries.
      const inside = modifiedScroll >= node.top && modifiedScroll < node.bottom;
      if (inside) {
        // Stop at the first we find, so only one is highlighted.
        highlightNode(node, true);
        return;
      }
    }
  }

  // Update nodes when the article is resized.
  const observer = new ResizeObserver((entries) => {
    updateNodesCache();
  });
  observer.observe(article);

  // Highlight the current section when the window is scrolled.
  window.addEventListener('scroll', () => {
    updateHighlightedNode();
  });
  updateHighlightedNode();
});
